<?php

use App\Controller\Obj;
use App\Controller\City;
use App\Controller\Text;
use App\Controller\Video;
use App\Controller\Reviews;
use App\Controller\Massmedia;

$ctrl = isset($_GET['ctrl'])?'App\Controller\\'.$_GET['ctrl']:'App\Controller\Index';

$controller = new $ctrl;
$objController=new Obj();
$cityController=new City();
$textController = new Text();
$videoController = new Video();
$reviewsController = new Reviews();
$massMediaController = new Massmedia();

$cityNow=$cityController->cityNow();
//var_dump($cityNow);
$phone=$cityNow->phone;
$addressCity=$cityNow->addressCity;
$addressStreet=$cityNow->addressStreet;
$email=$cityNow->email;

$rootPath = '/' . $cityNow->alias;
?>

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta property="og:image" content="/img/logomin.png" />
    <title><?php echo $this->data['pageTitle']?>ООО «Коллегия Бизнеса»</title>
    <link href="/css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/star-rating.min.css">
    <link rel="stylesheet" href="/css/style.css">
    <link rel="stylesheet" href="/css/bootstrap-select.min.css">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="/js/bootstrap.js"></script>
    <script src="/js/postBusForm.js"></script>
    <script src="/js/validForm.js"></script>
    <script src="/js/star-rating.min.js"></script>
    <script src="/js/search.js"></script>
    <script src="/js/script.js"></script>
    <script src="/js/videoReview.js"></script>
    <script src="/js/bootstrap-select.min.js"></script>
    <script src="/js/i18n/defaults-ru_RU.min.js"></script>
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="modal fade bs-example-modal-sm" id="buyBusiness" tabindex="-1" role="dialog" aria-labelledby="buyBusinessLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content modal-text">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="buyBusinessLabel">Хотите купить бизнес?</h4>
            </div>
            <div class="modal-body">
                <div class="alert alert-info">Оставьте заявку и наш менеджер свяжется с Вами!</div>
                <form class="form" role="form" name="BuyForm" id="BuyForm" >
                    <div class="form-group">
                        <label for="clientName">Ваше имя</label><span class="text-danger" style="float: right" id="nameError"></span>
                        <input type="text" class="form-control" id="clientName" name="clientName" placeholder="Иванов Иван Иванович" maxlength="50">
                    </div>
                    <div class="form-group">
                        <label for="clientPhone">Введите телефон</label><span class="text-danger" style="float: right" id="phoneError"></span>
                        <input type="text" class="form-control" id="clientPhone" name="clientPhone" placeholder="+7" maxlength="14">
                    </div>
                    <input type="submit" class="btn btn-inverse" value="Отправить" onclick="validBuyForm();return false;">
                    <div class="alert alert-success hidden" id="BuyFormOk"><strong>Спасибо, заявка отправлена!</strong> Наш менеджер свяжется с вами в течении 15 минут</div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade bs-example-modal-sm" id="sellModal" tabindex="-1" role="dialog" aria-labelledby="sellModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content modal-text">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="sellModalLabel">Хотите продать бизнес?</h4>
            </div>
            <div class="modal-body">
                <div class="alert alert-info">Оставьте заявку и наш менеджер свяжется с Вами!</div>
                <form class="form" role="form" name="sellForm" id="sellForm" >
                    <div class="form-group">
                        <label for="clientName">Ваше имя</label><span class="text-danger" style="float: right" id="nameErrorSellForm"></span>
                        <input type="text" class="form-control" id="clientNameSellForm" name="clientName" placeholder="Иванов Иван Иванович" maxlength="50">
                    </div>
                    <div class="form-group">
                        <label for="clientPhone">Введите телефон</label><span class="text-danger" style="float: right" id="phoneErrorSellForm"></span>
                        <input type="text" class="form-control" id="clientPhoneSellForm" name="clientPhone" placeholder="+7" maxlength="14">
                    </div>
                    <input type="submit" class="btn btn-inverse" value="Отправить" onclick="validSellForm();return false;">
                    <div class="alert alert-success hidden" id="sellFormOk"><strong>Спасибо, заявка отправлена!</strong> Наш менеджер свяжется с вами в течении 15 минут</div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="topBorderHeader">
    <div class="container">
        <div class="row">
            <div class="region">
                <?php $cityController->action_all();?>
            </div>
            <div style="margin-top: 3px;float: right;margin-right: 10px">
                <a href="https://vk.com/board_business" target="_blank"><img src="/img/social/vk.png" alt="vk.com" title="Наша группа в VK" width="25"></a>
                <a href="https://www.youtube.com/channel/UCJ4PrA7WIg6BrhWuDA3gEVg" target="_blank"><img src="/img/social/yt.png" alt="youtube.com" title="Наш канал на YouTube" width="25"></a>
                <a href="https://www.facebook.com/boardbusinessrf/" target="_blank"><img src="/img/social/fb.png" alt="facebook.com" title="Наша группа в FaceBook" width="25"></a>
                <a href="https://ok.ru/group/53015735238874" target="_blank"><img src="/img/social/ok.png" alt="ok.com" title="Мы в Одноклассниках" width="25"></a>
                <a href="https://www.instagram.com/board_business/" target="_blank"><img src="/img/social/inst.png" alt="instagram.com" title="Наш Instagram" width="25"></a>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row headerMainBox">
        <div class="col-md-3">
            <a href="<?php echo $rootPath?>"><img src="/img/logoblack.png" alt="Коллегия Бизнеса - Всероссийская коллегия бизнеса и инвестиций" width="100%"></a>
        </div>
        <div class="col-md-3 col-xs-6 headerInfoBox" style="padding: 0">
            <div class="col-sm-2 hidden-xs"><img src="/img/ico/map_marker.png" alt="..." width="25"></div>
            <div class="col-sm-10 col-xs-12" style="padding-right: 0">
                <p><b>г. <?php echo $addressCity?></b> <br><?php echo $addressStreet?></p>
            </div>
        </div>
        <div class="col-md-3 col-xs-6 headerInfoBox" style="padding: 0">
            <div class="col-sm-2 hidden-xs"><img src="/img/ico/phone_marker.png" alt="..." width="30"></div>
            <div class="col-sm-10 col-xs-12" style="padding-right: 0;">
                <p><b>Телефон:</b> <br><?php echo $phone?></p>
            </div>
        </div>
        <div class="col-md-3 headerInfoBox hidden-xs">
            <form action="<?php echo $rootPath?>/obj/find/" method="GET">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Поиск..." name="search" maxlength="20">
                        <span class="input-group-btn">
                            <button type="submit" class="btn btn-kb btn-default"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
                        </span>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="menu hidden-sm hidden-xs">
    <ul class="container">
        <li class="homeMenu"><a href="<?php echo $rootPath?>" style="color: #fff;">ГЛАВНАЯ</a></li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">ГОТОВЫЙ БИЗНЕС <span class="caret"></span></a>
            <ul class="dropdown-menu" style="margin-top: 16px">
                <li><a href="#">Продать</a></li>
                <li><a href="<?php echo $rootPath?>/obj/find">Купить</a></li>
                <li><a href="#">Сдать в адренду</a></li>
                <li><a href="#">Взять в адренду</a></li>
                <li><a href="<?php echo $rootPath?>/obj/sold">Продано / Сдано</a></li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">УСЛУГИ <span class="caret"></span></a>
            <ul class="dropdown-menu" style="margin-top: 16px">
                <li><a href="<?php echo $rootPath?>/service/view/1">Привлечение инвестиций</a></li>
                <li><a href="<?php echo $rootPath?>/service/view/2">Оптимизация бизнеса</a></li>
                <li><a href="<?php echo $rootPath?>/service/view/3">Комплексное сопровождение</a></li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">О КОМПАНИИ <span class="caret"></span></a>
            <ul class="dropdown-menu" style="margin-top: 16px">
                <li><a href="#">Контакты</a></li>
                <li><a href="#">История компании</a></li>
                <li><a href="<?php echo $rootPath?>/massmedia">Новости о нас</a></li>
                <li role="separator" class="divider"></li>
                <li><a href="#">Вакансии</a></li>
            </ul>
        </li>
        <li><a href="<?php echo $rootPath?>/reviews">ОТЗЫВЫ</a></li>
<!--        <li><a href="#">ПРОДАТЬ БИЗНЕС</a></li>-->

    </ul>
</div>

<nav class="navbar navbar-inverse hidden-md hidden-lg" role="navigation" style="margin-top: 10px">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Коллегия Бизнеса</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="homeMenu"><a href="<?php echo $rootPath?>" style="color: #fff;">ГЛАВНАЯ</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">ГОТОВЫЙ БИЗНЕС <span class="caret"></span></a>
                    <ul class="dropdown-menu" style="margin-top: 16px">
                        <li><a href="#">Продать</a></li>
                        <li><a href="<?php echo $rootPath?>/obj/find">Купить</a></li>
                        <li><a href="#">Сдать в адренду</a></li>
                        <li><a href="#">Взять в адренду</a></li>
                        <li><a href="<?php echo $rootPath?>/obj/sold">Продано / Сдано</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">УСЛУГИ <span class="caret"></span></a>
                    <ul class="dropdown-menu" style="margin-top: 16px">
                        <li><a href="<?php echo $rootPath?>/service/view/1">Привлечение инвестиций</a></li>
                        <li><a href="<?php echo $rootPath?>/service/view/2">Оптимизация бизнеса</a></li>
                        <li><a href="<?php echo $rootPath?>/service/view/3">Комплексное сопровождение</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">О КОМПАНИИ <span class="caret"></span></a>
                    <ul class="dropdown-menu" style="margin-top: 16px">
                        <li><a href="#">Контакты</a></li>
                        <li><a href="#">История компании</a></li>
                        <li><a href="<?php echo $rootPath?>/massmedia">Новости о нас</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="#">Вакансии</a></li>
                    </ul>
                </li>
                <li><a href="<?php echo $rootPath?>/reviews">ОТЗЫВЫ</a></li>
            </ul>
            <form action="business.php" method="GET" class="navbar-form navbar-left" role="search">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Поиск..." name="search" maxlength="20">
                    <span class="input-group-btn">
                        <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
                    </span>
                </div>
            </form>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
<div class="hello hidden-xs hidden-sm">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div style="margin: 0 auto;width: 900px;">
                    <div class="titleLeadBG">
                        <div class="titleLeadBox">
                            <h3 class="titleLead"><img src="/img/lead/icoTitleLead.png" width="45"> Мы готовы предложить Вам</h3>
                        </div>
                    </div>
                    <div class="leadBox">
                        <div class="headLeadBox" style="background-color: #fdae30;">
                            <h4>КУПЛЯ/ПРОДАЖА БИЗНЕСА</h4>
                        </div>
                        <div class="contentLeadBox">
                            <p><img src="/img/lead/kup-prod.png" alt=""></p>
                            <p>Полное сопровождение при купли/продажи готового бизнеса</p>
                            <div class="row">
                                <div class="col-xs-6" style="padding-right: 0;"><div class="buttonLeadBox" style="background-color: #45939c;margin-left: 5px"><a href="#" data-toggle="modal" data-target="#buyBusiness">КУПИТЬ</a></div></div>
                                <div class="col-xs-6" style="padding-left: 0;"><div class="buttonLeadBox" style="background-color: #41942e;margin-right: 5px;padding-left: 17px"><a href="#" data-toggle="modal" data-target="#sellModal">ПРОДАТЬ</a></div></div>
                                <div style="margin-left:90px;position: absolute;margin-top: -5px"><p><img src="/img/lead/button/or.png" width="40" alt=""></p></div>
                            </div>
                        </div>
                    </div>
                    <div class="leadBox">
                        <div class="headLeadBox" style="background-color: #45939c;">
                            <h4>ПРИВЛЕЧЕНИЕ ИНВЕСТИЦИЙ</h4>
                        </div>
                        <div class="contentLeadBox">
                            <p><img src="/img/lead/invest.png" alt=""></p>
                            <p>Привлечение инвесторов на готовый бизнес-план</p>
                            <div class="row">
                                <div class="col-xs-12">
                                    <p class="readMoreLeadBox">
                                        <a href="<?php echo $rootPath?>/service/view/1">ПОДРОБНЕЕ <span class="glyphicon glyphicon-circle-arrow-right" style="font-size: 11pt;"></span></a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="leadBox">
                        <div class="headLeadBox" style="background-color: #73ba92;">
                            <h4>ОПТИМИЗАЦИЯ БИЗНЕСА</h4>
                        </div>
                        <div class="contentLeadBox">
                            <p><img src="/img/lead/optim.png" alt=""></p>
                            <p>Качественная оптимизация бизнеса с гарантией дохода</p>
                            <div class="row">
                                <div class="col-xs-12">
                                    <p class="readMoreLeadBox">
                                        <a href="<?php echo $rootPath?>/service/view/2">ПОДРОБНЕЕ <span class="glyphicon glyphicon-circle-arrow-right" style="font-size: 11pt;"></span></a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="leadBox" style="margin-right: 0">
                        <div class="headLeadBox" style="background-color: #d84a36;">
                            <h4>КОМПЛЕКСНОЕ СОПРОВОЖДЕНИЕ</h4>
                        </div>
                        <div class="contentLeadBox">
                            <p><img src="/img/lead/sopr.png" alt=""></p>
                            <p>Комплексное сопровождение любого бизнеса</p>
                            <div class="row">
                                <div class="col-xs-12">
                                    <p class="readMoreLeadBox">
                                        <a href="<?php echo $rootPath?>/service/view/3">ПОДРОБНЕЕ <span class="glyphicon glyphicon-circle-arrow-right" style="font-size: 11pt;"></span></a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="buttonAllService">
                        <a href="#">ВСЕ УСЛУГИ <img src="/img/lead/arrowAllService.png" style="float: right"></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-8" style="padding-top: 10px;">
            <?php
            $this->display($this->data['display']);
            /*if (file_exists(__DIR__ . '/../view/' . $this->data['display'])) {
                include __DIR__ . '/../view/' . $this->data['display'];
            }*/
            ?>
        </div>

        <div class="col-md-4 visible-md visible-lg">
            <div class="block">
                <div class="headerBlock"><p>Хотите купить бизнес?</p></div>
                <div class="infoBlock">Оставьте заявку и наш менеджер свяжется с Вами!</div>
                <div class="mainBlock">
                    <form class="form" role="form" name="headerBuyForm" id="headerBuyForm">
                        <div class="form-group" id="divInputName">
                            <label class="sr-only" for="InputName">Ваше имя</label>
                            <input type="text" class="form-control" id="InputName" name="clientName" placeholder="Ваше имя" maxlength="50">
                        </div>
                        <div class="form-group" id="divInputPhone">
                            <label class="sr-only" for="InputPhone">Введите телефон</label>
                            <input type="text" class="form-control" id="InputPhone" name="clientPhone" placeholder="Введите телефон" maxlength="14">
                        </div>
                        <input type="submit" class="btn btn-inverse" value="Отправить" onclick="validHeadBuyForm();return false;"><span class="text-danger" style="float: right;margin-top: 5px" id="headerBuyFormError"></span><div class="text-success hidden" style="margin-top: 10px" id="headerBuyFormOk"><b>Спасибо, сообщение отправлено!</b></div>
                    </form>
                </div>
            </div>
            <button class="btn btn-success btn-lg" data-toggle="modal" data-target="#sellModal" style="width: 100%;margin: 15px 0"><i class="fa fa-plus-square-o" style="float: left;font-size: 36pt"></i> Добавить объявление <br><small>и продать бизнес</small></button>
<!--            <div class="block">-->
<!--                <div class="headerBlock"><p>Позвонить</p></div>-->
<!--                <div class="mainBlock">-->
<!--                    <div class="text-center">-->
<!--                        <img src="/img/girl.png" alt="..." width="200">-->
<!--                        <div style="font-size: 14pt">Звоните по любым вопросам:</div>-->
<!--                        <div style="font-size: 15pt;color: #00a1cb;"><span class="glyphicon glyphicon-earphone"></span> --><?php //echo $phone?><!--</div>-->
<!--                        <p></p>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
            <div class="block">
                <div class="headerBlock"><p>Жалоба / Отзыв</p></div>

                <div class="infoBlock" style="margin: 0">Вы хотите оставить отзыв?</div>
                <div class="mainBlock">
                    <button class="btn btn-success" data-toggle="modal" data-target="#reviewsModal" style="width: 100%"><i class="fa fa-smile-o" style="float: left;font-size: 16pt;"></i> Оставить отзыв</button>
                    <div class="modal fade bs-example-modal-sm" id="reviewsModal" tabindex="-1" role="dialog" aria-labelledby="reviewsModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content modal-text">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title" id="reviewsModalLabel">Отправить отзыв</h4>
                                </div>
                                <div class="modal-body">
                                    <img src="/img/formReviews.png" width="100%">
                                    <div class="alert alert-info">Наша компания следит за качеством
                                        предоставляемых услуг по продаже готового бизнеса,
                                        поэтому нам очень важно Ваше мнение!</div>
                                    <form class="form" role="form" name="reviewsForm" id="reviewsForm">
                                        <h3 class="h2title">Личные данные</h3>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="clientName">Ваше имя*</label><span class="text-danger" style="float: right" id="nameErrorReviewsForm"></span>
                                                    <input type="text" class="form-control" id="clientNameReviewsForm" name="clientName" placeholder="Иван">
                                                </div>
                                                <div class="form-group">
                                                    <label for="clientPhone">Ваш телефон*</label><span class="text-danger" style="float: right" id="phoneErrorReviewsForm"></span>
                                                    <input type="text" class="form-control" id="clientPhoneReviewsForm" name="clientPhone" placeholder="+7">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="surnameErrorReviewsForm">Ваша фамилия*</label><span class="text-danger" style="float: right" id="surnameErrorReviewsForm"></span>
                                                    <input type="text" class="form-control" id="clientSurnameReviewsForm" name="clientSurname" placeholder="Иванов">
                                                </div>
                                                <div class="form-group">
                                                    <label for="clientEmail">Ваш E-mail</label><span class="text-danger" style="float: right" id="emailErrorReviewsForm"></span>
                                                    <input type="text" class="form-control" id="clientEmailReviewsForm" name="clientEmail" placeholder="ivan@mail.ru">
                                                </div>
                                            </div>
                                        </div>
                                        <h3 class="h2title">Оцените нашу работу</h3>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="input-4" class="control-label">Общая оценка</label>
                                                    <input id="input-4" name="overallAssess" value="5" class="rating-loading" data-size="xs">
                                                    <table class="table-bordered table-condensed hidden">
                                                        <tr><th>Custom Clear Button</th><th>Caption Location</th></tr>
                                                        <tr><td><span id="kv-clear"></span></td><td><span id="kv-caption"></span></td></tr>
                                                    </table>
                                                    <script>
                                                        $(document).on('ready', function(){
                                                            $('#input-4').rating({
                                                                clearButton: '<button class="btn btn-default" type="button">' +
                                                                '<i class="glyphicon glyphicon-remove"></i> Clear</button>',
                                                                clearElement: "#kv-clear",
                                                                captionElement: "#kv-caption"
                                                            });
                                                        });
                                                    </script>
                                                </div>
                                            </div>
                                        </div>
                                        <h3 class="h2title">Текст отзыва* <span class="text-danger" style="float: right;font-size: 12pt" id="textReviewsError"></span></h3>
                                        <textarea class="form-control" rows="3" style="max-width: 100%;min-height: 100px;margin-bottom: 15px" id="textReviews" name="textReviews"></textarea>
                                        <input type="submit" class="btn btn-inverse" value="Отправить" onclick="validReviewsForm();return false;">
                                        <div class="alert alert-success hidden" id="reviewsFormOk"><strong>Спасибо!</strong> Отзыв отправлен!</div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="infoBlock" style="margin: 0">Вам не нравится работа компании?</div>
                <div class="mainBlock">
                    <button class="btn btn-danger" data-toggle="modal" data-target="#claimModal" style="width: 100%"><i class="fa fa-frown-o" style="float: left;font-size: 16pt;"></i> Пожаловаться директору</button>
                    <div class="modal fade bs-example-modal-sm" id="claimModal" tabindex="-1" role="dialog" aria-labelledby="claimModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content modal-text">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title" id="claimModalLabel">Отправить жалобу</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="alert alert-info">Наша компания следит за качеством
                                        предоставляемых услуг по продаже готового бизнеса,
                                        поэтому нам очень важно Ваше мнение!</div>
                                    <form class="form" role="form" name="claimForm" id="claimForm">
                                        <h3 class="h2title">Личные данные</h3>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="clientName">Ваше имя*</label><span class="text-danger" style="float: right" id="nameErrorClaimForm"></span>
                                                    <input type="text" class="form-control" id="clientNameClaimForm" name="clientName" placeholder="Иван">
                                                </div>
                                                <div class="form-group">
                                                    <label for="clientPhone">Ваш телефон*</label><span class="text-danger" style="float: right" id="phoneErrorClaimForm"></span>
                                                    <input type="text" class="form-control" id="clientPhoneClaimForm" name="clientPhone" placeholder="+7">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="clientSurnameClaimForm">Ваша фамилия*</label><span class="text-danger" style="float: right" id="clientSurnameError"></span>
                                                    <input type="text" class="form-control" id="clientSurnameClaimForm" name="clientSurname" placeholder="Иванов">
                                                </div>
                                                <div class="form-group">
                                                    <label for="clientEmail">Ваш E-mail</label><span class="text-danger" style="float: right" id="emailErrorClaimForm"></span>
                                                    <input type="text" class="form-control" id="clientEmailClaimForm" name="clientEmail" placeholder="ivan@mail.ru">
                                                </div>
                                            </div>
                                        </div>
                                        <h3 class="h2title">Объект претензии</h3>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="brokNameClaimForm">Имя брокера</label>
                                                    <input type="text" class="form-control" id="brokNameClaimForm" name="brokName" placeholder="Имя брокера">
                                                </div>
                                                <div class="form-group">
                                                    <label for="objClaim">Наименование объекта</label>
                                                    <input type="text" class="form-control" id="objClaim" name="objClaim" placeholder="Объект претензии">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="brokPhoneClaimForm">Телефон брокера</label>
                                                    <input type="text" class="form-control" id="brokPhoneClaimForm" name="brokPhone" placeholder="+7">
                                                </div>
                                                <div class="form-group">
                                                    <label for="costObj">Стоимость бизнеса</label>
                                                    <input type="text" class="form-control" id="costObj" name="costObj" placeholder="Стоимость">
                                                </div>
                                            </div>
                                        </div>
                                        <h3 class="h2title">Опишите проблему* <span class="text-danger" style="float: right;font-size: 12pt" id="textClaimError"></span></h3>
                                        <textarea class="form-control" rows="3" style="max-width: 100%;min-height: 100px;margin-bottom: 15px" id="textClaim" name="textClaim"></textarea>
                                        <input type="submit" class="btn btn-inverse" value="Отправить" onclick="validClaimForm();return false;">
                                        <div class="alert alert-success hidden" id="claimFormOk"><strong>Жалоба отправлена!</strong> Мы проанализируем Вашу проблему и при необходимости свяжемся с Вами для её решения</div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<!--            <div style="margin:15px 0;padding: 5px 0; font-size: 18pt;border-bottom: 1px solid #dddddd;border-top: 1px solid #dddddd;">-->
<!--                <a href="--><?php //echo $rootPath?><!--/video">Видео отзывы</a>-->
<!--            </div>-->
        <!--<div class="block">-->
            <!--<div class="mainBlock">-->
                    <?php $videoController->action_some(0,[],['id'=>'DESC'])?>
                <!--<a href="--><?php //echo $rootPath?><!--/video" class="btn btn-primary" style="width: 100%">Все видео</a>-->
            <!--</div>-->
        <!--</div>-->
            <div style="margin:15px 0;padding: 5px 0; font-size: 18pt;border-bottom: 1px solid #dddddd;border-top: 1px solid #dddddd;">
                <a href="<?php echo $rootPath?>/reviews">Отзывы</a>
            </div>
            <div class="block">
                <div class="mainBlock">
                    <?php $reviewsController->action_some(3,[],['id'=>'DESC'])?>
                    <a href="<?php echo $rootPath?>/reviews" class="btn btn-primary" style="width: 100%">Открыть все отзывы</a>
                </div>
            </div>
<!--            <div style="margin:15px 0;padding: 5px 0; font-size: 18pt;border-bottom: 1px solid #dddddd;border-top: 1px solid #dddddd;">-->
<!--                <a href="--><?php //echo $rootPath?><!--/massmedia">СМИ о нас</a>-->
<!--            </div>-->
<!--            <div class="block">-->
<!--                <div class="mainBlock">-->
                    <?php //$massMediaController->action_some(3,[],['id'=>'DESC'])?>
<!--                    <a href="--><?php //echo $rootPath?><!--/massmedia" class="btn btn-primary" style="width: 100%">Открыть все новости о нас</a>-->
<!--                </div>-->
<!--            </div>-->

            <div style="margin:15px 0;padding: 5px 0; font-size: 18pt;border-bottom: 1px solid #dddddd;border-top: 1px solid #dddddd;">
                <a href="<?php echo $rootPath?>/obj/sold">Мы продали</a>
            </div>
            <div class="block">
                <div class="row">
                    <?php $objController->action_someSold(3,['status'=>'1'],['id'=>'DESC'])?>
                    <div class="col-md-12"><a href="<?php echo $rootPath?>/obj/sold" class="btn btn-primary" style="width: 100%">Все проданные объекты</a></div>
                </div>
            </div>
        </div>

    </div>
    <?php
        //$controller->action_text();
    ?>

</div>
<div class="row footer">
    <div class="container">
        <div class="row">
            <div class="col-md-12" style="margin-bottom: 10px">Продажа готового бизнеса в Саратове. Прибыльный готовый бизнес от собственника. Юридическая защита сделки. Работаем с 2015 года. ООО «Всероссийская Коллегия Бизнеса и Инвестиций»</div>
        </div>
        <div class="row footerMenu">
            <div class="col-md-3 col-sm-4">
                <ul>
                    <li><span style="font-weight: 600"><a href="#">Наши услуги</a></span></li>
                    <li><a href="#">Продажа готового бизнеса</a></li>
                    <li><a href="#">Поиск рентабельного бизнеса</a></li>
                    <li><a href="#">Юридическое сопровождение сделок</a></li>
                    <li><a href="#">Привлечение инвесторов на готовый бизнес-план</a></li>
                    <li><a href="#">Покупка/продажа коммерческой недвижимости</a></li>
                    <li><a href="#">Всесторонняя проверка бизнеса</a></li>
                    <li><a href="#">Помощь клиентам на первое время</a></li>
                    <li><a href="#">Регистрация ИП</a></li>
                    <li><a href="#">Участие в тендерах</a></li>
                    <li><a href="#">Рос имущество</a></li>
                    <li><a href="#">Ликвидация ИП, юр. лиц</a></li>
                    <li><a href="#">Поиск бизнеса</a></li>
                    <li><a href="#">Поверка договоров и составление</a></li>
                    <li><a href="#">Регистрация товарных знаков</a></li>
                    <li><a href="#">Скупка имущества банкротов</a></li>
                    <li><a href="#">Оформление кредитов</a></li>
                </ul>
            </div>
            <div class="col-md-3 col-sm-4">
                <ul>
                    <li><span style="font-weight: 600"><a href="#">О компании</a></span></li>
                    <li><a href="#">Контакты</a></li>
                    <li><a href="#">История компании</a></li>
                    <li><a href="#">Новости о нас</a></li>
                    <li><a href="#">Вакансии</a></li>
                </ul>
            </div>
            <div class="col-md-3 col-sm-4">
                <ul>
                    <li><span style="font-weight: 600"><a href="#">Бизнес по сферам</a></span></li>
                    <li><a href="<?php echo $rootPath?>/obj/find/?selectCategory[]=1">Автобизнес</a></li>
                    <li><a href="<?php echo $rootPath?>/obj/find/?selectCategory[]=2">Гостиничный и отельный бизнес</a></li>
                    <li><a href="<?php echo $rootPath?>/obj/find/?selectCategory[]=3">Ресторанный бизнес</a></li>
                    <li><a href="<?php echo $rootPath?>/obj/find/?selectCategory[]=4">Медицинский бизнес</a></li>
                    <li><a href="<?php echo $rootPath?>/obj/find/?selectCategory[]=5">Производственный бизнес</a></li>
                    <li><a href="<?php echo $rootPath?>/obj/find/?selectCategory[]=6">Туристический бизнес</a></li>
                    <li><a href="<?php echo $rootPath?>/obj/find/?selectCategory[]=7">Бизнес в сфере торговли</a></li>
                    <li><a href="<?php echo $rootPath?>/obj/find/?selectCategory[]=8">Бизнес в интернете</a></li>
                    <li><a href="<?php echo $rootPath?>/obj/find/?selectCategory[]=9">Арендный бизнес</a></li>
                    <li><a href="<?php echo $rootPath?>/obj/find/?selectCategory[]=10">Сфера услуг</a></li>
                    <li><a href="<?php echo $rootPath?>/obj/find/?selectCategory[]=11">Автомойки</a></li>
                    <li><a href="<?php echo $rootPath?>/obj/find/?selectCategory[]=12">Торговля</a></li>
                    <li><a href="<?php echo $rootPath?>/obj/find/?selectCategory[]=13">Общественное питание</a></li>
                </ul>
            </div>
            <div class="col-md-3 col-sm-12" style="">
                <a href="/"><img src="/img/logomin.png" height="60" alt="..."></a>
                <address style="margin-top: 6px">
                    <strong>«Всероссийская Коллегия Бизнеса и Инвестиций»</strong><br>
                    г. <?php echo $addressCity?><br>
                    <?php echo $addressStreet?><br>
                    <abbr title="Телефон">Тел:</abbr> <?php echo $phone?><br>
                    <abbr title="Электронная почта">E-mail:</abbr> <?php echo $email?>
                </address>

            </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-center"><small>Copyright © 2016, Общество с ограниченной ответственностью «Всероссийская Коллегия Бизнеса и Инвестиций», официальный сайт</small></div>
        </div>
    </div>
</div>
<a href="#up" title="Вверх"><img id="buttonUp" src="/img/up-arrow-black-hi.png" alt="Up" width="50"></a>

<script type="text/javascript" src="/js/button-up.js" ></script>
</body>
</html>