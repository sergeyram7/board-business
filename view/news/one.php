<?php $news=$this->data['news'][0];
$pageTitle=$news->title;
?>
<div class="col-md-12" style="padding: 0">
    <div class="thumbnail" style="padding: 15px">
        <h3 class="h2title" style="margin-top: 0;margin-bottom: 15px;padding-bottom: 6px"><?php echo $news->title?></h3>
        <div class="row">
            <div class="col-md-12">
                <div class="col-xs-6"><i class="fa fa-calendar"></i> <?php echo $news->dateTime?></div>
                <div class="col-xs-6"><span style="float:right"><i class="fa fa-eye"></i> <?php echo $news->views?></span></div>
            </div>
        </div>
        <div class="row" style="margin-top: 15px">
            <div class="col-md-12">
            <a href="/news.php?id=<?php echo $news->id?>"><img src="/img/news/<?php echo $news->urlPhoto?>" alt="<?php echo $news->title?>" class="img-thumbnail" style="float:left;max-width:300px;margin-right:10px;margin-bottom:10px;"></a>
                <?php echo $news->longTextNews?>
            </div>
        </div>
    </div>
</div>