<?php
$path = $this->rootPath . '/news/view/';
?>
<h2 class="h2title" style="margin-bottom: 20px">Все новости</h2>
<?php foreach($this->data['news'] as $news):?>
    <div class="col-md-12" style="padding:0">
        <div class="thumbnail" style="padding: 15px">
            <h3 style="border-bottom: 1px solid #dddddd;margin-top: 0;padding-bottom: 5px"><a href="<?php echo $path?><?php echo $news->id?>" ><?php echo $news->title?></a></h3>
            <div class="row">
                <div class="col-sm-4"><a href="<?php echo $path?><?php echo $news->id?>"><img src="/img/news/<?php echo $news->urlPhoto?>" alt="<?php echo $news->title?>" width="100%"></a></div>
                <div class="col-sm-8"><p><?php echo $news->shortTextNews?></p></div>
            </div>
            <div class="row">
                <div class="col-md-4" style="padding-top: 8px"><span><i class="fa fa-calendar"></i> <?php echo $news->dateTime?></span><span style="margin-left: 20px"><i class="fa fa-eye"></i> <?php echo $news->views?></span></div>
                <div class="col-md-8"><a href="<?php echo $path?><?php echo $news->id?>" class="btn btn-success" style="float: right; font-weight: 600"><i class="fa fa-book"></i> Читать</a></div>
            </div>
        </div>
    </div>
<?php endforeach;?>
