<?php
use App\Controller\City;

$cityController=new City();
$cityNow=$cityController->cityNow();
$rootPath = '/' . $cityNow->alias;
?>
<h2 class="h2title">Найти бизнес</h2>
<form action="<?php echo $rootPath?>/obj/find/" method="GET">
    <div class="input-group">
        <input type="text" class="form-control" placeholder="Введите наименование бизнеса или франшизы" name="search">
        <span class="input-group-btn">
            <button class="btn btn-kb btn-default" type="submit"><span class="glyphicon glyphicon-search"></span></button>
        </span>
    </div>
</form>
<h2 class="h2title"><a href="<?php echo $rootPath?>/obj/find/?search=&filter=2">Новые объекты</a></h2>
<div class="row">
    <?php
        require __DIR__ . '/../obj/new.php';
    ?>
</div>
<div class="row"><div class="col-md-12"><a href="/business.php" class="btn btn-default" style="width: 100%">Все объекты
            (<?php echo $this->data['objCount'];?>)
        </a></div>
</div>
<h2 style="border-bottom: 1px solid #dddddd"><a href="<?php echo $rootPath?>/obj/find/?search=&priceFrom=5000000" style="color: #000">Объекты, стоимостью свыше 5 млн. руб.</a></h2>
<div class="row">
    <?php
        $this->data['obj'] = $this->data['objOver']; //перезапись для вывода объектов за 50млн
        require __DIR__ . '/../obj/all.php';
    ?>
</div>
<div class="row"><div class="col-md-12"><a href="<?php echo $rootPath?>/obj/find/?search=&priceFrom=5000000" class="btn btn-default" style="width: 100%">Все объекты, свыше 5 млн. руб. (<?php echo $this->data['objOverCount'];?>)</a></div>
</div>
<!--<h2 style="border-bottom: 1px solid #dddddd"><a href="--><?php //echo $this->rootPath?><!--/news" style="color: #000">Все новости</a></h2>-->
<!--<div class="row">-->
<!--        --><?php
//            require_once __DIR__.'/../news/simple.php'; //Вывод новостей
//        ?>
<!--    <div class="col-md-12"><a href="--><?php //echo $this->rootPath?><!--/news" class="btn btn-default" style="width: 100%">Все новости (--><?php //echo $this->data['newsCount'];?><!--)</a></div>-->
<!--</div>-->