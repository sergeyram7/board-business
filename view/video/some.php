<?php 
function getTitle($title){
    $outTitle = str_replace('\'', '\\\'' , $title);
    echo str_replace('\"', '\\\"' , $outTitle);
}
?>
<div class="modal fade" id="videoReview" tabindex="-1" role="dialog" aria-labelledby="videoReviewLabel" aria-hidden="false">
    <div class="modal-dialog">
        <div class="modal-content" id="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="videoReview.clean()">&times;</button>
                <h4 class="modal-title" id="videoReviewTitle"><!--title--></h4>
            </div>
            <div class="modal-body" id="videoReviewBody" style="padding: 0">
            </div>
        </div>
    </div>
    <script>videoReview.init()</script>
</div>
<?php foreach($this->data['video'] as $video):?>
<div style="border-bottom: 1px solid #dddddd;padding-bottom: 10px;margin-bottom: 15px">
    <a href="#" onclick="videoReview.load('<?php getTitle($video->title)?>','<?php echo $video->idYT?>')" data-toggle="modal" data-target="#videoReview">
    <img src="https://img.youtube.com/vi/<?php echo $video->idYT?>/0.jpg" width="100%" alt="...">
    <div><?php echo $video->title?></div></a>
    <span><i class="fa fa-eye"></i> <?php echo $video->views?></span>
</div>
<?php endforeach;?>