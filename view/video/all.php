<div class="modal fade" id="videoReview" tabindex="-1" role="dialog" aria-labelledby="videoReviewLabel" aria-hidden="false">
    <div class="modal-dialog">
        <div class="modal-content" id="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="videoReview.clean()">&times;</button>
                <h4 class="modal-title" id="videoReviewTitle"><!--title--></h4>
            </div>
            <div class="modal-body" id="videoReviewBody" style="padding: 0">
                <!-- content -->
            </div>
        </div>
    </div>
</div>

<h2 class="h2title">Все видео</h2>
<div class="row">
<?php foreach($this->data['video'] as $video):?>

    <div class="col-xs-12 col-sm-6">
        <div class="thumbnail" style="padding: 0">
            <a href="#" onclick="videoReview.load('<?php echo str_replace('\'', '\\\'' , $video->title)?>','<?php echo $video->idYT?>')" data-toggle="modal" data-target="#videoReview"><img src="https://img.youtube.com/vi/<?php echo $video->idYT?>/0.jpg" alt="<?php echo $video->title?>" width="100%"></a>
            <div class="caption">
                <div class="row"><div class="col-md-12"><span style="font-size: 14pt"><a href="#" onclick="videoReview.load('<?php echo str_replace('\'', '\\\'' , $video->title)?>','<?php echo $video->idYT?>')" data-toggle="modal" data-target="#videoReview" style="color: #000"><?php echo $video->title?></a></span></div></div>
                <span style="font-weight: 600;margin-top: 3px"><i class="fa fa-eye"></i> <?php echo $video->views?></span>
            </div>
        </div>
    </div>

<?php endforeach;?>
</div>
