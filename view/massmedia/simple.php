<?php
$path = $this->rootPath . '/massmedia/view/';
?>
<?php foreach($this->data['massmedia'] as $item):?>
    <div class="blockMMAboutUs">
        <div class="row">
            <div class="col-md-6">
                <img src="/img/smi/<?php echo $item->imgSource?>" alt="<?php echo $item->title?>" class="img-thumbnail" width="100%">
            </div>
            <div class="col-md-6" style="padding-left: 0">
                <div class="sourceMMAboutUs"><a href="<?php echo $item->linkSource?>"><?php echo $item->source?></a></div>
                <div class="pMMAboutUs"><b><?php echo $item->dateTime?></b></div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <p class="pMMAboutUs"><a href="<?php echo $path?><?php echo $item->id?>" style="color:#000;"><b><?php echo $item->title?></b></a></p>
                <p class="pMMAboutUs"><?php echo $item->shortTextMMAboutUs?></p>
            </div>
        </div>
        <a href="<?php echo $path?><?php echo $item->id?>" class="btn btn-default">Читать полностью</a>
    </div>
<?php endforeach;?>