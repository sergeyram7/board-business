<?php
$path = $this->rootPath . '/massmedia/view/';
?>
<h2 class="h2title" style="margin-bottom: 20px">Все упоминания СМИ о нас</h2>
<?php foreach($this->data['massmedia'] as $smi):?>
    <div class="blockMMAboutUs">
        <div class="row">
            <div class="col-xs-3">
                <img src="/img/smi/<?php echo $smi->imgSource?>" alt="<?php echo $smi->title?>" class="img-thumbnail" width="100%">
            </div>
            <div class="col-xs-9" style="padding-left: 0">
                <div class="sourceMMAboutUs"><a href="<?php echo $path?><?php echo $smi->id?>" style="color:#000;"><span class="pMMAboutUs" style="font-size:12pt"><b><?php echo $smi->title?></b></span></a></div>
                <div class="pMMAboutUs"><a href="<?php echo $smi->linkSource?>"><?php echo $smi->source?></a></div>
                <div class="pMMAboutUs"><b><?php echo $smi->dateTime?></b></div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <p class="pMMAboutUs"><?php echo $smi->shortTextMMAboutUs?></p>
            </div>
        </div>
        <a href="<?php echo $path?><?php echo $smi->id?>" class="btn btn-default">Читать полностью</a>
    </div>
<?php endforeach;?>
