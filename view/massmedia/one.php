<h3 class="h2title" style="margin-top: 0;margin-bottom: 15px;padding-bottom: 6px"><?php echo $this->data['massmedia']->title?></h3>
<div class="row">
    <div class="col-sm-3">
        <a href="/smi.php?id=<?php echo $this->data['massmedia']->id?>"><img src="/img/smi/<?php echo $this->data['massmedia']->imgSource?>" alt="<?php echo $this->data['massmedia']->title?>" class="img-thumbnail"></a>
    </div>
    <div class="col-sm-9">
        <div class="row" style="border-bottom: 1px solid #ccc;font-size: 12pt">
            <div class="col-md-6"><span style="font-weight: 600;color: #2c3e50"><a href="<?php echo $this->data['massmedia']->linkSource?>"><?php echo $this->data['massmedia']->source?></a></span> <br><?php echo $this->data['massmedia']->dateTime?></div>
        </div>
    </div>
</div>
<div class="row" style="margin-top: 15px">
    <div class="col-sm-12">
        <?php echo $this->data['massmedia']->longTextMMAboutUs?>
    </div>
</div>
