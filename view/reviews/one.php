<div class="thumbnail" style="padding: 15px">
    <h3 class="h2title" style="margin-top: 0;margin-bottom: 15px;padding-bottom: 6px"><?php echo $this->data['reviews']->title?></h3>
    <div class="row">
        <div class="col-sm-3" style="margin-bottom: 10px">
            <a href="/img/textreviews/<?php echo $this->data['reviews']->urlPhoto?>"><img src="/img/textreviews/<?php echo $this->data['reviews']->urlPhoto?>" alt="<?php echo $this->data['reviews']->title?>" class="img-thumbnail"></a>
            <div style="margin-top: 5px" ><script type="text/javascript" src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js" charset="utf-8"></script>
                <script type="text/javascript" src="//yastatic.net/share2/share.js" charset="utf-8"></script>
                <span>Поделиться:</span><div class="ya-share2" data-services="vkontakte,facebook,odnoklassniki,twitter"></div></div>
        </div>
        <div class="col-sm-6">
            <div class="row" style="border-bottom: 1px solid #ccc;font-size: 12pt">
                <div class="col-xs-6"><span style="font-weight: 600;color: #2c3e50"><?php echo $this->data['reviews']->authorName?></span> <br><?php echo $this->data['reviews']->dateTime?></div>
                <div class="col-xs-6"><span style="float: right">Брокер: <span style="font-weight: 600;color: #2c3e50"><?php echo $this->data['reviews']->brokName?></span></span></div>
            </div>
            <div class="row" style="margin-top: 15px">
                <div class="col-sm-12">
                    <?php echo $this->data['reviews']->longTextReviews?>
                </div>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="form-group">
                <label for="input-4" class="control-label">Общая оценка</label>
                <input id="input-4" name="overallAssess" value="<?php echo $this->data['reviews']->overallAssess?>" class="rating-loading" data-size="xs">
                <script>
                    $(document).on('ready', function(){
                        $('#input-4').rating({
                            displayOnly: true,
                            step: 0.5
                        });
                    });
                </script>
            </div>
        </div>
    </div>
</div>
