<?php
function cityUrl($alias){
    $path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
    $pathParts = explode('/', $path);
    $pathParts[1] = $alias;
    return implode('/', $pathParts);
}
?>

<button class="btn btn-link" data-toggle="modal" data-target="#chooseRegion">Регион: <span class="regionCity"><?php echo $this->data['cityNow']->name?></span> <span class="caret"></span>
</button>
<div class="modal fade bs-example-modal-sm" id="chooseRegion" tabindex="-1" role="dialog" aria-labelledby="chooseRegionLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content modal-text">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="chooseRegionLabel">Выберите город</h4>
            </div>
            <div class="modal-body">
                <?php foreach($this->data['items'] as $cityList):?>
                    <a href="<?php echo cityUrl($cityList->alias)?>" class="btn btn-link"><?php echo $cityList->name?></a>
                <?php endforeach;?>
            </div>
        </div>
    </div>
</div>