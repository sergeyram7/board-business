<?php
require_once __DIR__ . '/../../functions/obj.php';

$obj = $this->data['obj'][0];
?>
<div class="row">
    <div class="col-md-12">
        <div class="thumbnail" style="padding: 15px">
            <h3 class="h2title" style="margin-top: 0;margin-bottom: 15px;padding-bottom: 6px"><?php echo $obj->title?></h3>
            <div class="row">
                <div id="main_area">
                    <!-- Slider -->
                    <div class="row">

                        <div class="col-xs-10">
                            <div class="col-xs-12" id="slider">
                                <!-- Top part of the slider -->
                                <div class="row">
                                    <div class="col-sm-12" id="carousel-bounding-box">
                                        <div class="carousel slide" id="myCarousel">
                                            <!-- Carousel items -->
                                            <div class="carousel-inner">
                                                <div class="active item" data-slide-number="0">
                                                    <img src="/img/obj/<?php echo $obj->img?>"></div>

                                                <?php if($obj->addImg1){?>
                                                    <div class="item" data-slide-number="1">
                                                        <img src="/img/obj/<?php echo $obj->addImg1?>"></div>
                                                <?php }?>

                                                <?php if($obj->addImg2){?>
                                                    <div class="item" data-slide-number="2">
                                                        <img src="/img/obj/<?php echo $obj->addImg2?>"></div>
                                                <?php }?>

                                                <?php if($obj->addImg3){?>
                                                    <div class="item" data-slide-number="1">
                                                        <img src="/img/obj/<?php echo $obj->addImg3?>"></div>
                                                <?php }?>

                                                <?php if($obj->addImg4){?>
                                                    <div class="item" data-slide-number="1">
                                                        <img src="/img/obj/<?php echo $obj->addImg4?>"></div>
                                                <?php }?>
                                            </div>
                                            <!-- Carousel nav -->
                                            <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                                                <span class="glyphicon glyphicon-chevron-left"></span>
                                            </a>
                                            <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                                                <span class="glyphicon glyphicon-chevron-right"></span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-2" id="slider-thumbs" >
                            <!-- Bottom switcher of slider -->
                            <ul class="hide-bullets">
                                <li>
                                    <a class="thumbnail" id="carousel-selector-0">
                                        <img src="/img/obj/<?php echo $obj->img?>">
                                    </a>
                                </li>

                                <?php if($obj->addImg1){?>
                                    <li>
                                        <a class="thumbnail" id="carousel-selector-1">
                                            <img src="/img/obj/<?php echo $obj->addImg1?>">
                                        </a>
                                    </li>
                                <?php } else {?>
                                    <li>
                                        <a class="thumbnail" id="carousel-selector-1">
                                            <img src="/img/obj/min/noaddimg.png">
                                        </a>
                                    </li>
                                <?php }?>

                                <?php if($obj->addImg2){?>
                                    <li>
                                        <a class="thumbnail" id="carousel-selector-2">
                                            <img src="/img/obj/<?php echo $obj->addImg2?>">
                                        </a>
                                    </li>
                                <?php } else {?>
                                    <li>
                                        <a class="thumbnail" id="carousel-selector-2">
                                            <img src="/img/obj/min/noaddimg.png">
                                        </a>
                                    </li>
                                <?php }?>

                                <?php if($obj->addImg3){?>
                                    <li>
                                        <a class="thumbnail" id="carousel-selector-3">
                                            <img src="/img/obj/<?php echo $obj->addImg3?>">
                                        </a>
                                    </li>
                                <?php } else {?>
                                    <li>
                                        <a class="thumbnail" id="carousel-selector-3">
                                            <img src="/img/obj/min/noaddimg.png">
                                        </a>
                                    </li>
                                <?php }?>

                                <?php if($obj->addImg4){?>
                                    <li>
                                        <a class="thumbnail" id="carousel-selector-4">
                                            <img src="/img/obj/<?php echo $obj->addImg4?>">
                                            </a>
                                    </li>
                                <?php } else {?>
                                    <li>
                                        <a class="thumbnail" id="carousel-selector-4">
                                            <img src="/img/obj/min/noaddimg.png">
                                        </a>
                                    </li>
                                <?php }?>

                            </ul>
                        </div>
                        <!--/Slider-->
                    </div>
                </div>
                <div class="col-sm-10">
                    <h3 style="margin-top: 10px">Основная информация</h3>
                    <table class="table table-striped table-hover tableMainInfo" style="">
                        <thead>
                        <tr>
                            <th></th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>Цена:</td>
                            <td style="color: #3e8f3e;font-weight: 600;text-align: right;">
                                <?php priceFormat($obj->cost)?><i class="fa fa-rub"></i>
                            </td>
                        </tr>
                        <tr>
                            <td>Прибыль:</td>
                            <td style="color: #337ab6;font-weight: 600;text-align: right;">
                                <?php priceFormat($obj->income)?><i class="fa fa-rub"></i>
                            </td>
                        </tr>
                        <tr>
                            <td>Окупаемость:</td>
                            <td style="text-align: right;"><?php echo (int)($obj->cost/$obj->income);?> месяцев</td>
                        </tr>
                        <tr>
                            <td>Категория:</td>
                            <td style="text-align: right;">
                                <?php
                                echo ($this->data['category'][0]->name);
                                ?>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <div class="row">
                        <?php if($obj->status==1) echo '<div class="alert alert-danger"><strong>Объект был продан!</strong></div>'?>
                        <div class="col-sm-6">
                            <button class="btn btn-primary" <?php if($obj->status==1) echo 'disabled="disabled" '?>style="width: 100%;margin-bottom:10px" data-toggle="modal" data-target="#buyBusiness"><span class="glyphicon glyphicon-usd"></span> Купить бизнес</button>
                        </div>
                        <div class="col-sm-6">
                            <button class="btn btn-success" <?php if($obj->status==1) echo 'disabled="disabled" '?>style="width: 100%;margin-bottom:10px" data-toggle="modal" data-target="#getDoc"><span class="glyphicon glyphicon-folder-open"></span>  Получить пакет документов</button>
                            <div class="modal fade bs-example-modal-sm" id="getDoc" tabindex="-1" role="dialog" aria-labelledby="getDocLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content modal-text">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="getDocLabel">Получить полный пакет документов</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="alert alert-info">Компания Коллегия Бизнеса защищает данные своих клиентов, поэтому нам потребуется информация о вас, чтобы предоставить Вам полный пакет документов. Оставьте заявку и наш менеджер свяжется с Вами!</div>
                                            <form class="form" role="form" name="docForm" id="docForm" >
                                                <div class="form-group">
                                                    <label for="clientName">Ваше имя</label><span class="text-danger" style="float: right" id="docNameError"></span>
                                                    <input type="text" class="form-control" id="docClientName" name="clientName" placeholder="Иванов Иван Иванович">
                                                </div>
                                                <div class="form-group">
                                                    <label for="clientPhone">Введите телефон</label><span class="text-danger" style="float: right" id="docPhoneError"></span>
                                                    <input type="text" class="form-control" id="docClientPhone" name="clientPhone" placeholder="+7">
                                                </div>
                                                <div class="form-group">
                                                    <label for="docClientEmail">Введите E-mail</label><span class="text-danger" style="float: right" id="docEmailError"></span>
                                                    <input type="text" class="form-control" id="docClientEmail" name="clientEmail" placeholder="ivan@mail.ru">
                                                </div>
                                                <input type="submit" class="btn btn-inverse" value="Отправить" onclick="validDocForm();return false;">
                                                <div class="alert alert-success hidden" id="docFormOk"><strong>Заявка отправлена!</strong> Наш менеджер свяжется с вами в течении 30 минут</div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Поделиться -->
                        <div style="margin-left: 15px" ><script type="text/javascript" src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js" charset="utf-8"></script>
                            <script type="text/javascript" src="//yastatic.net/share2/share.js" charset="utf-8"></script>
                            <span>Поделиться:</span><div class="ya-share2" data-services="vkontakte,facebook,odnoklassniki,twitter"></div></div>
                    </div>
                </div>
                <div class="col-sm-2" style="padding-left: 5px">
                    <?php if($obj->icoCenterCity){?>
                        <div class="text-center ico">
                            <img src="/img/ico/icoCity.png" alt="Центр города" title="Центр города" width="60">
                            <span>Центр города<br></span>
                        </div>
                    <?php }?>
                    <?php if($obj->icoClientsBase){?>
                        <div class="text-center ico">
                            <img src="/img/ico/icoClients.png" alt="Наработанная клиентская база" title="Наработанная клиентская база" width="60">
                            <span>Клиентская база</span>
                        </div>
                    <?php }?>
                    <?php if($obj->icoStableProfit){?>
                        <div class="text-center ico">
                            <img src="/img/ico/icoChart.png" alt="Стабильная прибыль" title="Стабильная прибыль" width="60">
                            <span>Стабильная прибыль</span>
                        </div>
                    <?php }?>
                    <?php if($obj->icoLiqBus){?>
                        <div class="text-center ico">
                            <img src="/img/ico/icoLiq.png" alt="Ликвидный бизнес" title="Ликвидный бизнес" width="60">
                            <span>Ликвидный бизнес</span>
                        </div>
                    <?php }?>
                    <?php if($obj->icoPriceAssets){?>
                        <div class="text-center ico">
                            <div><img src="/img/ico/icoActiv.png" alt="По цене активов" title="По цене активов" width="60"></div>
                            <span>По цене активов</span>
                        </div>
                    <?php }?>
                    <?php if($obj->icoSuperRentable){?>
                        <div class="text-center ico">
                            <div><img src="/img/ico/icoRent.png" alt="Сверхрентабельно" title="Сверхрентабельно" width="60"></div>
                            <span>Сверх- рентабельно</span>
                        </div>
                    <?php }?>
                </div>
            </div>
        </div>
        <div class="thumbnail" style="padding: 15px">
            <h3 class="h2title" style="margin-top: 0;margin-bottom: 15px;padding-bottom: 6px">
                Информация об этом бизнесе: <br><small><?php echo $obj->title?></small></h3>
            <div class="row">
                <div class="col-md-12">
                    <?php echo $obj->textObj?>
                </div>
            </div>
        </div>
        <div class="thumbnail" style="padding: 15px">
            <div class="row">
                <div class="col-md-12">
                    <h3 style="margin-top: 0">Местоположение</h3>
                    <table class="table table-striped table-hover tableMainInfo">
                        <thead>
                        <tr>
                            <th style="width: 50%"></th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>Город:</td>
                            <td>
                                <?php
                                echo $this->data['city'][0]->name;
                                ?>
                            </td>
                        </tr>
                        <?php if($obj->area_id){?>
                        <tr>
                            <td>Район:</td>
                            <td>
                                <?php
                                echo ($this->data['area'][0]->name);
                                ?>
                            </td>
                        </tr>
                        <?php }?>
                        <?php if($obj->region){?>
                            <tr>
                                <td>Место рядом:</td>
                                <td><?php echo $obj->region;?></td>
                            </tr>
                        <?php }?>
                        </tbody>
                    </table>
                    <h3 style="margin-top: 0">Финансовое состояние</h3>
                    <table class="table table-striped table-hover tableMainInfo">
                        <thead>
                        <tr>
                            <th style="width: 50%"></th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>Цена:</td>
                            <td style="color: #3e8f3e;font-weight: 600;"><?php priceFormat($obj->cost)?><i class="fa fa-rub"></i></td>
                        </tr>
                        <tr>
                            <td>Прибыль:</td>
                            <td style="color: #337ab6;font-weight: 600;"><?php priceFormat($obj->income)?><i class="fa fa-rub"></i></td>
                        </tr>
                        <?php if($obj->monthRevs){?>
                            <tr>
                                <td>Среднемесячные обороты:</td>
                                <td>
                                    <?php priceFormat($obj->monthRevs)?><i class="fa fa-rub"></i>
                                </td>
                            </tr>
                        <?php }?>
                        <?php if($obj->monthCosts){?>
                            <tr>
                                <td>Среднемесячные расходы:</td>
                                <td><?php priceFormat($obj->monthCosts)?><i class="fa fa-rub"></i></td>
                            </tr>
                        <?php }?>
                        </tbody>
                    </table>
                    <h3 style="margin-top: 0">Информация о штате компании</h3>
                    <table class="table table-striped table-hover tableMainInfo">
                        <thead>
                        <tr>
                            <th style="width: 50%"></th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if($obj->qtCompStaff){?>
                            <tr>
                                <td>Количество работников:</td>
                                <td><?php echo $obj->qtCompStaff;?></td>
                            </tr>
                        <?php }?>
                        <?php if($obj->listCompStaff){?>
                            <tr>
                                <td>Список персонала:</td>
                                <td><?php echo $obj->listCompStaff;?></td>
                            </tr>
                        <?php }?>
                        <?php if($obj->wagesFund){?>
                            <tr>
                                <td>Фонд з/п:</td>
                                <td><?php priceFormat($obj->wagesFund)?></td>
                            </tr>
                        <?php }?>
                        </tbody>
                    </table>
                    <h3 style="margin-top: 0">Информация о помещениях</h3>
                    <table class="table table-striped table-hover tableMainInfo">
                        <thead>
                        <tr>
                            <th style="width: 50%"></th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if($obj->spaceRooms){?>
                            <tr>
                                <td>Площадь помещений:</td>
                                <td><?php echo $obj->spaceRooms;?> / м2</td>
                            </tr>
                        <?php }?>
                        <?php if($obj->landlordInfo){?>
                            <tr>
                                <td>Информация об арендодателе:</td>
                                <td><?php echo $obj->landlordInfo;?></td>
                            </tr>
                        <?php }?>
                        <?php if($obj->addInfoRooms){?>
                            <tr>
                                <td>Дополнительная информация о помещениях:</td>
                                <td><?php echo $obj->addInfoRooms;?></td>
                            </tr>
                        <?php }?>
                        <?php if($obj->rentPrice){?>
                            <tr>
                                <td>Стоимость аренды:</td>
                                <td><?php priceFormat($obj->rentPrice)?><i class="fa fa-rub"></i> / мес.</td>
                            </tr>
                        <?php }?>
                        </tbody>
                    </table>
                    <h3 style="margin-top: 0">Основные средства</h3>
                    <table class="table table-striped table-hover tableMainInfo">
                        <thead>
                        <tr>
                            <th style="width: 50%"></th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if($obj->intangibles){?>
                            <tr>
                                <td>Нематериальные активы:</td>
                                <td><?php echo $obj->intangibles;?></td>
                            </tr>
                        <?php }?>
                        <?php if($obj->capitalGoods){?>
                            <tr>
                                <td>Средства производства:</td>
                                <td><?php echo $obj->capitalGoods;?></td>
                            </tr>
                        <?php }?>
                        </tbody>
                    </table>
                    <h3 style="margin-top: 0">Дополнительная информация</h3>
                    <table class="table table-striped table-hover tableMainInfo">
                        <thead>
                        <tr>
                            <th style="width: 50%"></th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if($obj->ageBus){?>
                            <tr>
                                <td>Возраст бизнеса:</td>
                                <td><?php echo $obj->ageBus;?></td>
                            </tr>
                        <?php }?>
                        <?php if($obj->formEntity){?>
                            <tr>
                                <td>Организационно-правовая форма:</td>
                                <td><?php echo $obj->formEntity;?></td>
                            </tr>
                        <?php }?>
                        <?php if($obj->docAndLicens){?>
                            <tr>
                                <td>Документы и лицензии:</td>
                                <td><?php echo $obj->docAndLicens;?></td>
                            </tr>
                        <?php }?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>