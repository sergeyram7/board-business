<?php foreach($this->data['obj'] as $obj):?>
<div class="col-xs-12 col-sm-6 obj">
    <div class="thumbnail" style="padding:0">
        <a href="/obj.php?id=' . $row[id] . '"><div class="imgObj"><img src="/img/obj/<?php echo $obj->img?>" alt="<?php echo $obj->title?>" height="100%"></div></a>
        <div class="label_new2"></div>
        <div class="caption">
            <div class="ribbon ribbon_green"><span><i class="fa fa-line-chart"></i> Окупаемость: <?php echo (int)($obj->cost/$obj->income)?> мес.</span></div>
            <div class="row"><div class="col-md-12"><span style="font-size: 14pt"><a href="/obj.php?id=<?php echo $obj->id?>" style="color: #000" title="<?php echo $obj->title?>"><script>var title="<?php echo $obj->title?>";if(title.length>="53"){document.write(title.substr(0,53)+"...");} else document.write(title);</script></a></span><span style="float: right;color: #999999;font-weight: 600;margin-top: 3px"><span class="glyphicon glyphicon-map-marker"></span> <?php echo \App\Model\City::getOne($obj->city_id)[0]->name?></span></div></div>
            <div class="row" style="margin-top: 5px">
                <div class="col-xs-6">
                    <img src="/img/label/prace.png" alt="..." width="35px" style="float: left;margin-right: 6px"><p>Цена: <br><span style="font-weight: 600;color: #337ab6"><script>cost="<?php echo $obj->cost?>"; cost= cost.split( /(?=(?:\d{3})+$)/ );for(i=0;cost[i];i++){document.write(cost[i]);document.write(" ");}</script><i class="fa fa-rub"></i></span></p>

                </div>
                <div class="col-xs-6">
                    <img src="/img/label/prib.png" alt="..." width="35px" style="float: left;margin-right: 6px"><p>Прибыль: <br><span style="font-weight: 600;color: #5cb75c"><script>income="<?php echo $obj->income?>"; income= income.split( /(?=(?:\d{3})+$)/ );for(i=0;income[i];i++){document.write(income[i]);document.write(" ");}</script><i class="fa fa-rub"></i></span></p>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-6"><a href="/obj.php?id=<?php echo $obj->id?>" class="btn btn-success" style="width: 100%"><b>Подробнее...</b></a></div>
                <div class="col-xs-6 text-center" style="color:#5cb75c;margin: 0 0;font-weight: 600;font-size:9pt"><span class="glyphicon glyphicon-usd"></span> Доходность: <?php echo (int)(($obj->income/$obj->cost)*12*100)?>% годовых</div>
            </div>
        </div>
    </div>
</div>
<?php endforeach;?>