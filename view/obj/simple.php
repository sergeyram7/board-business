<?php
require_once __DIR__ . '/../../functions/obj.php';

$path = $this->rootPath . '/obj/view/';
?>
<?php foreach($this->data['obj'] as $obj):?>
<div class="col-md-12 obj">
    <div class="thumbnail" style="padding:0">
        <a href="<?php echo $path . $obj->id?>"><div class="imgObj"><img src="/img/obj/<?php echo $obj->img?>" alt="<?php echo $obj->title?>" height="100%"></div></a>
        <div class="label_sold"></div>
        <div class="caption">
            <div class="ribbon ribbon_green"><span><i class="fa fa-line-chart"></i> Окупаемость: <?php echo (int)($obj->cost/$obj->income)?> мес.</span></div>
            <div class="row"><div class="col-md-12"><span style="font-size: 14pt"><a href="<?php echo $path . $obj->id?>" style="color: #000" title="<?php echo $obj->title?>"><?php shortTitle($obj->title)?></a></span><span style="float: right;color: #999999;font-weight: 600;margin-top: 3px"><span class="glyphicon glyphicon-map-marker"></span> <?php echo \App\Model\City::getOne($obj->city_id)[0]->name?></span></div></div>
            <div class="row" style="margin-top: 5px">
                <div class="col-xs-6">
                    <img src="/img/label/prace.png" alt="..." width="35px" style="float: left;margin-right: 6px"><p>Цена: <br><span style="font-weight: 600;color: #337ab6"><?php priceFormat($obj->cost)?><i class="fa fa-rub"></i></span></p>

                </div>
                <div class="col-xs-6">
                    <img src="/img/label/prib.png" alt="..." width="35px" style="float: left;margin-right: 6px"><p>Прибыль: <br><span style="font-weight: 600;color: #5cb75c"><?php priceFormat($obj->income)?><i class="fa fa-rub"></i></span></p>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-6"><a href="<?php echo $path . $obj->id?>" class="btn btn-success" style="width: 100%"><b>Подробнее...</b></a></div>
                <div class="col-xs-6 text-center" style="color:#5cb75c;margin: 0 0;font-weight: 600;font-size:9pt"><span class="glyphicon glyphicon-usd"></span> Доходность: <?php echo (int)(($obj->income/$obj->cost)*12*100)?>% годовых</div>
            </div>
        </div>
    </div>
</div>
<?php endforeach;?>