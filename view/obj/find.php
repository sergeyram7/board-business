<?php
    //$obj = $this->data['obj']
require_once __DIR__ . '/../../functions/obj.php';

    $path = $this->rootPath . '/obj/view/';

?>


<h2 class="h2title"><span id="begin">Каталог - бизнес в продаже</span></h2>
<div class="thumbnail" style="margin-bottom: 15px;padding: 15px">
    <h3 class="h2title" style="margin-top:0">Поиск по параметрам</h3>
    <form action="<?php echo $path?>#begin" method="GET" id="searchForm">
        <div class="row">
            <div class="col-md-12" style="margin-bottom: 6px">
                <label>Поиск</label>
                <input type="text" class="form-control" id="search_box" name="search" placeholder="Введите название бизнеса или id" value="<?php if(isset($_GET['search'])) echo $_GET['search'];?>">
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <label>Город</label>
                <select class="selectpicker" data-style="btn-primary" data-width="100%" id="selectCity" name="selectCity" onchange="outArea(value)">
                    <?php
                    foreach($this->data['cityFind'] as $city){
                        if((isset($_GET['selectCity']) && $_GET['selectCity'] == $city->id)){
                            echo "<option selected value=\"{$city->id}\">{$city->name}</option>";
                        } else {echo "<option value=\"{$city->id}\">{$city->name}</option>";}
                    }?>
                </select>
            </div>
            <div class="col-md-4" id="selectArea">
                <label>Район</label>
                <div id="selectAreaList">
                    <select class="selectpicker" data-style="btn-primary" multiple data-width="100%" title="Все районы" data-actions-box="true" name="selectArea[]">
                    </select>
                    <?php if(isset($_GET['selectCity'])) {
                        echo'<script>outArea('.$_GET['selectCity'].')</script>';
                        if(isset($_GET['selectArea'])){
                            $selectAreaList='';
                            foreach ($_GET['selectArea'] as $tempArea)
                                $selectAreaList.="'".$tempArea."',";
                            $selectAreaList.="''";
                            echo"<script>
                                            //функция отображает выбранные ранее районы
                                            function selectPointArea(){
                                            $('.selectpicker#selectArea').selectpicker('val', [{$selectAreaList}]);}
                                            </script>";}
                    }

                    ?>
                </div>
            </div>
            <div class="col-md-5" id="selectCategory">
                <label>Категория</label>
                <select class="selectpicker" title="Все категории" multiple data-width="100%" data-actions-box="true" name="selectCategory[]">

                    <?php
                    foreach($this->data['category'] as $category){/*
                        if (!isset($_GET['selectCategory'])) {
                            foreach ($_GET['selectCategory'] as $selectCategory) {
                                if ($selectCategory == $category){
                                    echo "<option selected value=\"{$category->id}\">{$category->name}</option>";
                                    break;
                                }
                            }
                            echo "<option value=\"{$category->id}\">{$category->name}</option>";
                        } else {
                            echo "<option value=\"{$category->id}\">{$category->name}</option>";
                        }*/
                        echo '<option ';
                        if (isset($_GET['selectCategory'])) {
                            foreach ($_GET['selectCategory'] as $selectCategory) {
                                if ($selectCategory == $category->id) echo 'selected ';
                            }
                        }
                        echo " value=\"{$category->id}\">{$category->name}</option>";
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="row" style="margin-top: 10px" id="filterRange">
            <div class="col-md-3">
                <label>Цена</label>
                <div class="row">
                    <div class="col-md-12">
                        <div class="input-group">
                            <input type="text" class="form-control" id="priceFrom" name="priceFrom" value="<?php if(isset($_GET['priceFrom'])) echo $_GET['priceFrom'];?>" placeholder="от:" maxlength="15">
                            <span class="input-group-btn"><span style="font-size: 16pt;margin: 0 2px">-</span></span>
                            <input type="text" class="form-control" id="priceTo" name="priceTo" value="<?php if (isset($_GET['priceTo'])) {
                                echo $_GET['priceTo'];
                            }?>" placeholder="до:" maxlength="15">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <label>Прибыль/Мес</label>
                <div class="row">
                    <div class="col-md-12">
                        <div class="input-group">
                            <input type="text" class="form-control" id="profitFrom" name="profitFrom" value="<?php if (isset($_GET['profitFrom'])) {
                                echo $_GET['profitFrom'];
                            }?>" placeholder="от:" maxlength="15">
                            <span class="input-group-btn"><span style="font-size: 16pt;margin: 0 2px">-</span></span>
                            <input type="text" class="form-control" id="profitTo" name="profitTo" value="<?php if (isset($_GET['profitTo'])) {
                                echo $_GET['profitTo'];
                            }?>" placeholder="до:" maxlength="15">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <label>Окупаемость/Мес</label>
                <div class="row">
                    <div class="col-md-12">
                        <div class="input-group">
                            <input type="text" class="form-control" id="returnFrom" name="returnFrom" value="<?php if (isset($_GET['returnFrom'])) {
                                echo $_GET['returnFrom'];
                            }?>" placeholder="от:" maxlength="15">
                            <span class="input-group-btn"><span style="font-size: 16pt;margin: 0 2px">-</span></span>
                            <input type="text" class="form-control" id="returnTo" name="returnTo" value="<?php if (isset($_GET['returnTo'])) {
                                echo $_GET['returnTo'];
                            }?>" placeholder="до:" maxlength="15">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <label>Обороты/Мес</label>
                <div class="row">
                    <div class="col-md-12">
                        <div class="input-group">
                            <input type="text" class="form-control" id="revsFrom" name="revsFrom" value="<?php if (isset($_GET['revsFrom'])) {
                                echo $_GET['revsFrom'];
                            }?>" placeholder="от:" maxlength="15">
                            <span class="input-group-btn"><span style="font-size: 16pt;margin: 0 2px">-</span></span>
                            <input type="text" class="form-control" id="revsTo" name="revsTo" value="<?php if (isset($_GET['revsTo'])) {
                                echo $_GET['revsTo'];
                            }?>" placeholder="до:" maxlength="15">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            document.getElementById("filterRange").onkeypress = function(e) {
                e = e || event;
                if (e.ctrlKey || e.altKey || e.metaKey) return;
                var chr = getChar(e);
                if (chr == null) return;
                if (chr < '0' || chr > '9') {
                    return false;
                }
            }
            function getChar(event) {
                if (event.which == null) {
                    if (event.keyCode < 32) return null;
                    return String.fromCharCode(event.keyCode) // IE
                }

                if (event.which != 0 && event.charCode != 0) {
                    if (event.which < 32) return null;
                    return String.fromCharCode(event.which) // остальные
                }

                return null; // специальная клавиша
            }
        </script>
        <div class="row" style="margin-top: 10px">
            <div class="col-md-9" style="border-top:1px solid #ddd;font-size: 11pt;padding-top:12px;">
                <span style="color: #888">Сортировать по:</span>
                <span style="margin-right: 10px"><button type="submit" class="btn btn-link" name="filter" value="<?php if(isset($_GET['filter'])&&$_GET['filter']==1) echo 2; else echo 1?>" style="padding: 0">Новизне <span class="caret" <?php if(isset($_GET['filter'])&&$_GET['filter']==1) echo 'style="transform: rotate(180deg);"';?></span></button></span>
                <span style="margin-right: 10px"><button type="submit" class="btn btn-link" name="filter" value="<?php if(isset($_GET['filter'])&&$_GET['filter']==3) echo 4; else echo 3?>" style="padding: 0">Цене <span class="caret" <?php if(isset($_GET['filter'])&&$_GET['filter']==3) echo 'style="transform: rotate(180deg);"';?>></span></button></span>
                <span style="margin-right: 10px"><button type="submit" class="btn btn-link" name="filter" value="<?php if(isset($_GET['filter'])&&$_GET['filter']==5) echo 6; else echo 5?>" style="padding: 0">Окупаемости <span class="caret" <?php if(isset($_GET['filter'])&&$_GET['filter']==5) echo 'style="transform: rotate(180deg);"';?>></span></button></span>
                <span style="margin-right: 10px"><button type="submit" class="btn btn-link" name="filter" value="<?php if(isset($_GET['filter'])&&$_GET['filter']==7) echo 8; else echo 7?>" style="padding: 0">Чистой прибыли <span class="caret" <?php if(isset($_GET['filter'])&&$_GET['filter']==7) echo 'style="transform: rotate(180deg);"';?>></span></button></span>
            </div>
            <div class="col-md-3"><input type="submit" class="btn btn-primary search_button" style="width: 100%;font-weight: 600" value="Показать"/></div>
        </div>
    </form>
</div>

<?php foreach($this->data['obj'] as $obj):?>
    <div class="col-xs-12 col-sm-6 obj">
        <div class="thumbnail" style="padding:0">
            <a href="<?php echo $path . $obj->id?>"><div class="imgObj"><img src="/img/obj/<?php echo $obj->img?>" alt="<?php echo $obj->title?>" height="100%"></div></a>
            <div class="caption">
                <div class="ribbon ribbon_green"><span><i class="fa fa-line-chart"></i> Окупаемость: <?php echo (int)($obj->cost/$obj->income)?> мес.</span></div>
                <div class="row"><div class="col-md-12"><span style="font-size: 14pt"><a href="<?php echo $path . $obj->id?>" style="color: #000" title="<?php echo $obj->title?>"><?php shortTitle($obj->title)?></a></span><span style="float: right;color: #999999;font-weight: 600;margin-top: 3px"><span class="glyphicon glyphicon-map-marker"></span> <?php echo \App\Model\City::getOne($obj->city_id)[0]->name?></span></div></div>
                <div class="row" style="margin-top: 5px">
                    <div class="col-xs-6">
                        <img src="/img/label/prace.png" alt="..." width="35px" style="float: left;margin-right: 6px"><p>Цена: <br><span style="font-weight: 600;color: #337ab6"><?php priceFormat($obj->cost)?><i class="fa fa-rub"></i></span></p>

                    </div>
                    <div class="col-xs-6">
                        <img src="/img/label/prib.png" alt="..." width="35px" style="float: left;margin-right: 6px"><p>Прибыль: <br><span style="font-weight: 600;color: #5cb75c"><?php priceFormat($obj->income)?><i class="fa fa-rub"></i></span></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-6"><a href="<?php echo $path . $obj->id?>" class="btn btn-success" style="width: 100%"><b>Подробнее...</b></a></div>
                    <div class="col-xs-6 text-center" style="color:#5cb75c;margin: 0 0;font-weight: 600;font-size:9pt"><span class="glyphicon glyphicon-usd"></span> Доходность: <?php echo (int)(($obj->income/$obj->cost)*12*100)?>% годовых</div>
                </div>
            </div>
        </div>
    </div>
<?php endforeach;?>