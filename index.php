<?php
    header('Content-Type: text/html; charset=utf-8');
    ini_set("display_errors",1);
    error_reporting(E_ALL);

    require_once __DIR__ . '/autoload.php';

    $path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
    $pathParts = explode('/', $path);

    $city = !empty($pathParts[1]) ? ucfirst($pathParts[1]):'all';


    $ctrl = !empty($pathParts[2]) ? 'App\Controller\\'.ucfirst($pathParts[2]):'App\Controller\Index';
    //var_dump($ctrl);

    $action = !empty($pathParts[3]) ? 'action_'.ucfirst($pathParts[3]):'action_all';
    if(isset($_GET['id'])) $action = 'action_view';
    //var_dump($action);

    $controller=new $ctrl;
    $controller->$action();
?>


