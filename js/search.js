/**
 * Created by Sergey Ramazanov on 20.04.2016.
 */
function outArea(city_id,selectAreaList){
    var data="city="+city_id;
    $.ajax({
        url:     "/../script/getArea.php", //Адрес подгружаемой страницы
        type:     "POST", //Тип запроса
        dataType: "html", //Тип данных
        data: data,
        beforeSend: function(html) { // запустится до вызова запроса
            $("#selectAreaList").html('');
        },
        success: function(html) { //Если все нормально
            $("#selectAreaList").append(html);// вывод
            $('.selectpicker').selectpicker('render'); //render для bootstrap-select
            selectPointArea();
        },
        error: function(response) { //Если ошибка
            alert('error!');
            //console.log(html);
        }
    });
}