var buttonUp = document.getElementById('buttonUp');

buttonUp.onmouseover = function() { // добавить прозрачность
    buttonUp.style.opacity=0.9;
    buttonUp.style.filter  = 'alpha(opacity=90)';
};

buttonUp.onmouseout = function() { //убрать прозрачность
    buttonUp.style.opacity = 0.6;
    buttonUp.style.filter  = 'alpha(opacity=60)';
};

buttonUp.onclick = function() { //обработка клика
    window.scrollTo(0,0);
};

document.onscroll = function(){
    if(window.pageYOffset > 400){
        //buttonUp.style.display = 'block';
        $('#buttonUp').fadeIn(1000);
    } else {
        //buttonUp.style.display = 'none';
        $('#buttonUp').fadeOut(1000);
    }
}