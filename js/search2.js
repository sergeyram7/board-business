/**
 * Created by Sergey Ramazanov on 20.04.2016.
 */
$(function() {
    $(".search_button2").click(function() {
        // получаем то, что написал пользователь
        var searchString = $("#search_box").val();
        // поиск по городу
        var city = '';
        if($("select#selectCity").val()!=0){city=' AND city_id='+$("select#selectCity").val();}
        // район
        var area = '';
        if($('#selectArea option:selected').val()){
            area = ' AND area IN (';
            $('#selectArea option:selected').each(function(){
                area+='\''+$(this).val()+'\'';
                area+=',';
            });
            area+='\'\')';
        } else {area = '';};
        // категории
        var category = '';
        if($('#selectCategory option:selected').val()){
            category = ' AND category IN (';
            $('#selectCategory option:selected').each(function(){
                category+='\''+$(this).val()+'\'';
                category+=',';
            });
            category+='\'\')';
        } else {category = '';};
        // цена от и до
        var priceFrom = '';
        var priceTo = '';
        if($('#priceFrom').val()){
            priceFrom = ' AND cost>='+$('#priceFrom').val();
        } else {priceFrom = '';};
        if($('#priceTo').val()){
            priceTo += ' AND cost<='+$('#priceTo').val();
        } else {priceTo = '';};
        // прибыль от и до
        var profitFrom = '';
        var profitTo = '';
        if($('#profitFrom').val()){
            profitFrom = ' AND income>='+$('#profitFrom').val();
        } else {profitFrom = '';};
        if($('#profitTo').val()){
            profitTo += ' AND income<='+$('#profitTo').val();
        } else {profitTo = '';};
        // окупаемость от и до
        var returnFrom = '';
        var returnTo = '';
        if($('#returnFrom').val()){
            returnFrom = ' AND cost/income>='+$('#returnFrom').val();
        } else {returnFrom = '';};
        if($('#returnTo').val()){
            returnTo += ' AND cost/income<='+$('#returnTo').val();
        } else {returnTo = '';};
        // обороты от и до
        var revsFrom = '';
        var revsTo = '';
        if($('#revsFrom').val()){
            revsFrom = ' AND monthRevs>='+$('#revsFrom').val();
        } else {revsFrom = '';};
        if($('#revsTo').val()){
            revsTo += ' AND monthRevs<='+$('#revsTo').val();
        } else {revsTo = '';};
        alert("SELECT * FROM obj WHERE title LIKE '%"+searchString+"%' OR id LIKE '%"+searchString+"%'"+city+area+category+priceFrom+priceTo+profitFrom+profitTo+returnFrom+returnTo+revsFrom+revsTo+"");
        resSearch(searchString,city,area,category,priceFrom,priceTo,profitFrom,profitTo,returnFrom,returnTo,revsFrom,revsTo);
        return false;
    });
});
function resSearch(searchString,city,area,category,priceFrom,priceTo,profitFrom,profitTo,returnFrom,returnTo,revsFrom,revsTo) {
    var data            = 'search='+ searchString;
    $.ajax({
        url:     "http://sitekb.for-test-only.ru/script/do_search.php", //Адрес подгружаемой страницы
        type:     "POST", //Тип запроса
        dataType: "html", //Тип данных
        //data: jQuery("#"+searchString+city+area+category+priceFrom+priceTo+profitFrom+profitTo+returnFrom+returnTo+revsFrom+revsTo),
        data: data,
        beforeSend: function(html) { // запустится до вызова запроса
            $("#searchResults").html('');
            $("#searchresults").show();
            $(".word").html(searchString);
        },
        success: function(html) { //Если все нормально
            alert('good!');
            $("#searchResults").show();
            $("#searchResults").append(html);
        },
        error: function(response) { //Если ошибка
            alert('error!');
        }
    });
    return false;
}