/**
 * Created by Sergey Ramazanov on 12.04.2016.
 */
function validBuyForm(){
    var name=document.forms["BuyForm"]["clientName"].value;
    var phone=document.forms["BuyForm"]["clientPhone"].value;
    //var email=document.forms["BuyForm"]["clientEmail"].value;

    if(name.length==0){
        document.getElementById("nameError").innerHTML="*заполните все поля";
        return false;
    } else {
        document.getElementById("nameError").innerHTML="";
    }
    if(phone.length==0){
        document.getElementById("phoneError").innerHTML="*заполните все поля";
        return false;
    } else {
        document.getElementById("phoneError").innerHTML="";
    }
    /*if(email.length==0){
        document.getElementById("emailError").innerHTML="*заполните все поля";
        return false;
    } else {
        document.getElementById("emailError").innerHTML="";
    }
    var at=email.indexOf("@");
    var dot=email.indexOf(".");
    if(at<1||dot<1){
        document.getElementById("emailError").innerHTML="*E-mail введен не верно";
        return false;
    } else {
        document.getElementById("emailError").innerHTML="";
    }*/
    postForm('BuyFormOk', 'BuyForm', '/script/buyForm.php');
    alert("Спасибо, наш менеджер свяжется с Вами в течении 15 минут.");
}
function validDocForm(){
    var name=document.forms["docForm"]["docClientName"].value;
    var phone=document.forms["docForm"]["docClientPhone"].value;
    var email=document.forms["docForm"]["docClientEmail"].value;

    if(name.length==0){
        document.getElementById("docNameError").innerHTML="*заполните все поля";
        return false;
    } else {
        document.getElementById("docNameError").innerHTML="";
    }
    if(phone.length==0){
        document.getElementById("docPhoneError").innerHTML="*заполните все поля";
        return false;
    } else {
        document.getElementById("docPhoneError").innerHTML="";
    }
    if(email.length==0){
     document.getElementById("docEmailError").innerHTML="*заполните все поля";
     return false;
     } else {
     document.getElementById("docEmailError").innerHTML="";
     }
     var at=email.indexOf("@");
     var dot=email.indexOf(".");
     if(at<1||dot<1){
     document.getElementById("docEmailError").innerHTML="*E-mail введен не верно";
     return false;
     } else {
     document.getElementById("docEmailError").innerHTML="";
     }
    postForm('docFormOk', 'docForm', '/script/docForm.php');
    alert("Спасибо, наш менеджер свяжется с Вами в течении 15 минут.");
}
function validHeadBuyForm(){
    var name=document.forms["headerBuyForm"]["InputName"].value;
    var phone=document.forms["headerBuyForm"]["InputPhone"].value;
    //var email=document.forms["headerBuyForm"]["InputEmail"].value;

    if(name.length==0){
        document.getElementById("divInputName").classList.add("has-error", "has-feedback");
        document.getElementById("headerBuyFormError").innerHTML="<b>*заполните все поля</b>";
        return false;
    } else {
        document.getElementById("divInputName").classList.remove("has-error", "has-feedback");
    }
    if(phone.length==0){
        document.getElementById("divInputPhone").classList.add("has-error", "has-feedback");
        document.getElementById("headerBuyFormError").innerHTML="<b>*заполните все поля</b>";
        return false;
    } else {
        document.getElementById("divInputPhone").classList.remove("has-error", "has-feedback");
    }
    /*if(email.length==0){
        document.getElementById("divInputEmail").classList.add("has-error", "has-feedback");
        document.getElementById("headerBuyFormError").innerHTML="<b>*заполните все поля</b>";
        return false;
    } else {
        document.getElementById("divInputEmail").classList.remove("has-error", "has-feedback");
    }
    var at=email.indexOf("@");
    var dot=email.indexOf(".");
    if(at<1||dot<1){
        document.getElementById("headerBuyFormError").innerHTML="<b>*E-mail введен не верно</b>";
        return false;
    } else {
        document.getElementById("headerBuyFormError").innerHTML="";
    }*/
    document.getElementById("headerBuyFormError").innerHTML="";
    postForm('headerBuyFormOk', 'headerBuyForm', '/script/buyForm.php');
    alert("Спасибо, наш менеджер свяжется с Вами в течении 15 минут.");
}
function validSellForm(){
    var name=document.forms["sellForm"]["clientNameSellForm"].value;
    var phone=document.forms["sellForm"]["clientPhoneSellForm"].value;
    //var email=document.forms["sellForm"]["clientEmailSellForm"].value;

    if(name.length==0){
        document.getElementById("nameErrorSellForm").innerHTML="*заполните все поля";
        return false;
    } else {
        document.getElementById("nameErrorSellForm").innerHTML="";
    }
    if(phone.length==0){
        document.getElementById("phoneErrorSellForm").innerHTML="*заполните все поля";
        return false;
    } else {
        document.getElementById("phoneErrorSellForm").innerHTML="";
    }
    /*if(email.length==0){
        document.getElementById("emailErrorSellForm").innerHTML="*заполните все поля";
        return false;
    } else {
        document.getElementById("emailErrorSellForm").innerHTML="";
    }
    var at=email.indexOf("@");
    var dot=email.indexOf(".");
    if(at<1||dot<1){
        document.getElementById("emailErrorSellForm").innerHTML="*E-mail введен не верно";
        return false;
    } else {
        document.getElementById("emailErrorSellForm").innerHTML="";
    }*/
    postForm('sellFormOk', 'sellForm', '/script/sellForm.php');
    alert("Спасибо, наш менеджер свяжется с Вами в течении 15 минут.");
}
function validHeadSellForm(){
    var name=document.forms["headerSellForm"]["clientName"].value;
    var phone=document.forms["headerSellForm"]["clientPhone"].value;
    var email=document.forms["headerSellForm"]["clientEmail"].value;

    if(name.length==0){
        document.getElementById("divSellInputName").classList.add("has-error", "has-feedback");
        document.getElementById("headerSellFormError").innerHTML="<b>*заполните все поля</b>";
        return false;
    } else {
        document.getElementById("divSellInputName").classList.remove("has-error", "has-feedback");
    }
    if(phone.length==0){
        document.getElementById("divSellInputPhone").classList.add("has-error", "has-feedback");
        document.getElementById("headerSellFormError").innerHTML="<b>*заполните все поля</b>";
        return false;
    } else {
        document.getElementById("divSellInputPhone").classList.remove("has-error", "has-feedback");
    }
    /*if(email.length==0){
        document.getElementById("divSellInputEmail").classList.add("has-error", "has-feedback");
        document.getElementById("headerSellFormError").innerHTML="<b>*заполните все поля</b>";
        return false;
    } else {
        document.getElementById("divSellInputEmail").classList.remove("has-error", "has-feedback");
    }
    var at=email.indexOf("@");
    var dot=email.indexOf(".");
    if(at<1||dot<1){
        document.getElementById("headerSellFormError").innerHTML="<b>*E-mail введен не верно</b>";
        return false;
    } else {
        document.getElementById("headerSellFormError").innerHTML="";
    }*/
    document.getElementById("headerSellFormError").innerHTML="";
    postForm('headerSellFormOk', 'headerSellForm', '/script/sellForm.php');
    alert("Спасибо, наш менеджер свяжется с Вами в течении 15 минут.");
}
function validClaimForm(){
    var clientName=document.forms["claimForm"]["clientNameClaimForm"].value;
    var clientSurname=document.forms["claimForm"]["clientSurnameClaimForm"].value;
    var clientPhone=document.forms["claimForm"]["clientPhoneClaimForm"].value;
    var clientEmail=document.forms["claimForm"]["clientEmailClaimForm"].value;
    var textClaim=document.forms["claimForm"]["textClaim"].value;

    if(clientName.length==0){
        document.getElementById("nameErrorClaimForm").innerHTML="*заполните все поля";
        return false;
    } else {
        document.getElementById("nameErrorClaimForm").innerHTML="";
    }
    if(clientSurname.length==0){
        document.getElementById("clientSurnameError").innerHTML="*заполните все поля";
        return false;
    } else {
        document.getElementById("clientSurnameError").innerHTML="";
    }
    if(clientPhone.length==0){
        document.getElementById("phoneErrorClaimForm").innerHTML="*заполните все поля";
        return false;
    } else {
        document.getElementById("phoneErrorClaimForm").innerHTML="";
    }
    if(clientEmail.length!=0){
        var at=clientEmail.indexOf("@");
        var dot=clientEmail.indexOf(".");
        if(at<1||dot<1){
            document.getElementById("emailErrorClaimForm").innerHTML="*E-mail введен не верно";
            return false;
        } else {
            document.getElementById("emailErrorClaimForm").innerHTML="";
        }
    }
    if(textClaim.length==0){
        document.getElementById("textClaimError").innerHTML="*заполните все поля";
        return false;
    } else {
        document.getElementById("textClaimError").innerHTML="";
    }

    postForm('claimFormOk', 'claimForm', '/script/claimForm.php');
    alert("Жалоба отправлена! Мы проанализируем Вашу проблему и при необходимости свяжемся с Вами для её решения");
}
function validReviewsForm(){
    var clientName=document.forms["reviewsForm"]["clientNameReviewsForm"].value;
    var clientSurname=document.forms["reviewsForm"]["clientSurnameReviewsForm"].value;
    var clientPhone=document.forms["reviewsForm"]["clientPhoneReviewsForm"].value;
    var clientEmail=document.forms["reviewsForm"]["clientEmailReviewsForm"].value;
    var textReviews=document.forms["reviewsForm"]["textReviews"].value;

    if(clientName.length==0){
        document.getElementById("nameErrorReviewsForm").innerHTML="*заполните все поля";
        return false;
    } else {
        document.getElementById("nameErrorReviewsForm").innerHTML="";
    }
    if(clientSurname.length==0){
        document.getElementById("surnameErrorReviewsForm").innerHTML="*заполните все поля";
        return false;
    } else {
        document.getElementById("surnameErrorReviewsForm").innerHTML="";
    }
    if(clientPhone.length==0){
        document.getElementById("phoneErrorReviewsForm").innerHTML="*заполните все поля";
        return false;
    } else {
        document.getElementById("phoneErrorReviewsForm").innerHTML="";
    }
    if(clientEmail.length!=0){
        var at=clientEmail.indexOf("@");
        var dot=clientEmail.indexOf(".");
        if(at<1||dot<1){
            document.getElementById("emailErrorReviewsForm").innerHTML="*E-mail введен не верно";
            return false;
        } else {
            document.getElementById("emailErrorReviewsForm").innerHTML="";
        }
    }
    if(textReviews.length==0){
        document.getElementById("textReviewsError").innerHTML="*заполните все поля";
        return false;
    } else {
        document.getElementById("textReviewsError").innerHTML="";
    }

    postForm('reviewsFormOk', 'reviewsForm', '/script/reviewsForm.php');
    alert("Спасибо! Ваш отзыв был отправлен.");
}