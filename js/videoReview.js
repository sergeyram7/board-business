var videoReview = {
    init: function () {
        this.modalTitle = document.getElementById('videoReviewTitle');
        this.modalBody = document.getElementById('videoReviewBody');
    },
    load: function (title, id) {
        this.modalTitle.innerHTML = title;
        this.modalBody.innerHTML = '<iframe width="100%" height="' + this.fixHeight() +'" id="video' + id + '" src="https://www.youtube.com/embed/' + id + '" frameborder="0" allowfullscreen></iframe>';
        //this.fixHeight();
    },
    clean: function () {
        this.modalTitle.innerHTML = '';
        this.modalBody.innerHTML = '';
    },
    fixHeight: function () {
        var width = document.body.clientWidth;
        var height;
        //max frame 730
        if(width>=768){
            height = 598/(16/9);
            return height;
        } else {
            height = (width-22)/(16/9);
            return height;
        }
    }
};