<?php

namespace App\Controller;
use App\Model\Text as TextModel;
use App\View\View;

class Text
{
    public function action_all($where=[],$sorting=[]){
        if(!empty($_GET['city'])) {
            $cityNowId = \App\Model\City::getBy('alias',$_GET['city'])->id;
            $where['city'] = $cityNowId;
        } else {
            $where['city']=0;
        }
        $items = TextModel::getBy('city',$where['city']);
        if(!$items) $items = TextModel::getBy('city','0');
        if($items!=null) {
            $view = new View();
            $view->assign('text', $items);
            $view->display('text/main.php');
        }
    }

}