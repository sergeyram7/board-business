<?php

namespace App\Controller;
use App\Model\News as NewsModel;
use App\Model\Text as TextModel;
use App\View\View;

class News
{
    public function action_all($where=[],$sorting=[]){
        $items = NewsModel::getAll($where,$sorting);
        $view = new View();
        $view->assign('news', $items);
        $view->assign('pageTitle', 'Новости :: ');
        $view->assign('display', 'news/all.php');
        $view->template();
        /*$view->assign('news', $items);
        $view->display('news/all.php');*/
    }
    public function action_view(){
        $items = NewsModel::getOne((int)$_GET['id']);
        $view = new View();
        $view->assign('news', $items);
        $view->assign('pageTitle', $items[0]->title . ' :: Новости :: ');
        $view->assign('display', 'news/one.php');
        $view->template();
        //$view->display('news/one.php');
    }

    public function action_some($num,$where=[],$sorting=[]){
        $items = NewsModel::getSome((int)$num,$where,$sorting);
        $view = new View();
        $view->assign('news', $items);
        $view->display('news/simple.php');
    }
    public function action_count(){
        return NewsModel::getNumAll();
    }

    public function action_text(){
        if(!empty($_GET['city'])) {
            $cityNowId = \App\Model\City::getBy('alias',$_GET['city'])->id;
            $where['city'] = $cityNowId;
        } else {
            $where['city']=0;
        }
        $items = TextModel::getBy('city',$where['city']);
        if(!$items) $items = TextModel::getBy('city','0');
        if ($items!=null) {
            $view = new View();
            $view->assign('text', $items);
            $view->display('text/main.php');
        }
    }

}