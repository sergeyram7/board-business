<?php

namespace App\Controller;
use App\Model\Service as ServiceModel;
use App\View\View;

class Service
{
    public function action_view(){
        $items = ServiceModel::getOne((int)$_GET['id']);
        $view = new View();
        $view->assign('serv', $items);
        $view->assign('pageTitle', $items[0]->title . ' :: Услуги :: ');
        $view->assign('display', 'service/one.php');
        $view->template();
        //$view->display('news/one.php');
    }
}