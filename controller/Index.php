<?php

namespace App\Controller;
use App\View\View;
use App\Model\News;
use App\Model\Obj;

class Index
{
    public function action_all($where=[]){
        $cityId=new City();
        $cityId = $cityId->cityNow()->id;
        if(empty($where)) $where=[];
        if($cityId!=0)$where['city_id'] = $cityId;
        //var_dump($where);
        $where['status']=0;
        $whereOver=$where;
        $whereOver['cost>']=5000000;
        $news = News::getSome(4,[],['id'=>'DESC']);
        $newsCount = News::getNumAll();
        $obj = Obj::getSome(10,$where,['id'=>'DESC']);
        $objCount = Obj::getNumAll($where);
        $objOver = Obj::getSome(10,$whereOver,['id'=>'DESC']);
        $objOverCount = Obj::getNumAll($whereOver);
        $view = new View();
        $view->assign('news',$news);
        $view->assign('newsCount',$newsCount);
        $view->assign('obj',$obj);
        $view->assign('objCount',$objCount);
        $view->assign('objOver',$objOver);
        $view->assign('objOverCount',$objOverCount);

        $view->assign('pageTitle', 'Главная страница :: ');
        $view->assign('display', 'index/main.php');
        $view->template();

        //$view->display('index/main.php');
    }
    public function action_text(){
        $view = new View();
        $view->display('index/text.php');
    }
}