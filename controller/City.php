<?php

namespace App\Controller;

use App\Model\City as CityModel;
use App\View\View;

class City
{
    public function action_all(){
        $cityAll = CityModel::getAll();
        $cityNow = isset($_GET['city'])?CityModel::getBy('alias',$_GET['city']):CityModel::getBy('alias','all');
        $view = new View();
        $view->assign('items', $cityAll);
        $view->assign('cityNow', $cityNow);
        $view->display('city/select.php');
    }
    public function cityNow(){
        return isset($_GET['city'])?CityModel::getBy('alias',$_GET['city']):CityModel::getBy('alias','all');
    }
}