<?php

namespace App\Controller;
use App\Model\Category;
use App\View\View;
use App\Model\Obj as ObjModel;
use App\Model\Category as CategoryModel;
use App\Model\City as CityModel;
use App\Model\Area as AreaModel;
use App\Model\Text as TextModel;

class Obj
{
    public function action_all($where=[],$sorting=[]){
        $cityId=new City();
        $cityId = $cityId->cityNow()->id;
        if(empty($where)) $where=[];
        if($cityId!=0)$where['city_id'] = $cityId;
        $items = ObjModel::getAll($where,$sorting);
        $view = new View();
        $view->assign('obj', $items);
        $view->display('obj/all.php');
    }
    public function action_some($num,$where=[],$sorting=[]){
        $cityId=new City();
        $cityId = $cityId->cityNow()->id;
        if(empty($where)) $where=[];
        if($cityId!=0)$where['city_id'] = $cityId;
        $items = ObjModel::getSome((int)$num,$where,$sorting);
        $view = new View();
        $view->assign('obj', $items);
        $view->display('obj/all.php');
    }
    public function action_find($numItems=[],$where=[],$sorting=[],$like=[]){
        //var_dump($_GET);
        $numItems['num'] = 10; //число элементов на странице
        if(isset($_GET['page'])){
            $numItems['from'] = (int)($_GET['page']*$numItems['num']-$numItems['num']);
        } else {
            $numItems['from'] = 0;
        }
        /**
         * TODO: оптимизировать
         */
        if (!empty($_GET['search'])) {
            $like['title'] = $_GET['search'];
        }
        if (!empty($_GET['selectCity'])) {
            $where['city_id'] = $_GET['selectCity'];
        }
        if (!empty($_GET['selectArea'])) {
            $where['area_id'] = $_GET['selectArea'];
        }
        if (!empty($_GET['selectCategory'])) {
            $where['category'] = $_GET['selectCategory'];
        }
        if (!empty($_GET['priceFrom'])) {
            $where['cost>'] = (int)$_GET['priceFrom'];
        }
        if (!empty($_GET['priceTo'])) {
            $where['cost<'] = (int)$_GET['priceTo'];
        }
        if (!empty($_GET['profitFrom'])) {
            $where['income>'] = (int)$_GET['profitFrom'];
        }
        if (!empty($_GET['profitTo'])) {
            $where['income<'] = (int)$_GET['profitTo'];
        }
        if (!empty($_GET['returnFrom'])) {
            $where['cost/income>'] = (int)$_GET['returnFrom'];
        }
        if (!empty($_GET['returnTo'])) {
            $where['cost/income<'] = (int)$_GET['returnTo'];
        }
        if (!empty($_GET['revsFrom'])) {
            $where['monthRevs>'] = (int)$_GET['revsFrom'];
        }
        if (!empty($_GET['revsTo'])) {
            $where['monthRevs<'] = (int)$_GET['revsTo'];
        }
        if (!empty($_GET['filter'])) {
            if ($_GET['filter']==1) {
                $sorting['dateTime'] = 'ASC';
            } elseif ($_GET['filter']==2){
                $sorting['dateTime'] = 'DESC';
            } elseif ($_GET['filter']==3){
                $sorting['cost'] = 'ASC';
            } elseif ($_GET['filter']==4){
                $sorting['cost'] = 'DESC';
            } elseif ($_GET['filter']==5){
                $sorting['cost/income'] = 'ASC';
            } elseif ($_GET['filter']==6){
                $sorting['cost/income'] = 'DESC';
            }  elseif ($_GET['filter']==5){
                $sorting['income'] = 'ASC';
            } elseif ($_GET['filter']==6){
                $sorting['income'] = 'DESC';
            }
        }
        $items = ObjModel::getRow($numItems,$where,$sorting,$like);
        $citySelect = CityModel::getAll();
        $categorySelect = CategoryModel::getAll();

        $view = new View();
        $view->assign('obj', $items);
        $view->assign('cityFind', $citySelect);
        $view->assign('category', $categorySelect);
        $view->assign('pageTitle', 'Готовый бизнес :: ');
        $view->assign('display', 'obj/find.php');
        $view->template();
    }
    public function action_view(){
        $items = ObjModel::getOne((int)$_GET['id']);
        $category = CategoryModel::getOne($items[0]->category);
        $area = AreaModel::getOne((int)$items[0]->area_id);
        $city = CityModel::getOne((int)$items[0]->city_id);
        $view = new View();
        $view->assign('obj', $items);
        $view->assign('category', $category);
        $view->assign('city', $city);
        $view->assign('area', $area);
        $view->assign('pageTitle', $items[0]->title . ' :: Объект :: ');
        $view->assign('display', 'obj/one.php');
        $view->template();
    }
    public function action_sold(){
        $cityId=new City();
        $cityId = $cityId->cityNow()->id;
        if(empty($where)) $where=[];
        if($cityId!=0)$where['city_id'] = $cityId;
        $where['status'] = 1;
        $sorting = ['id'=>'DESC'];
        $items = ObjModel::getSome(10,$where,$sorting);
        $view = new View();
        $view->assign('obj', $items);
        $view->assign('pageTitle', 'Проданные объекты :: ');
        $view->assign('display', 'obj/sold.php');
        $view->template();
    }

    public function action_someSold($num,$where=[],$sorting=[]){
        $cityId=new City();
        $cityId = $cityId->cityNow()->id;
        if(empty($where)) $where=[];
        if($cityId!=0)$where['city_id'] = $cityId;
        $items = ObjModel::getSome((int)$num,$where,$sorting);
        $view = new View();
        $view->assign('obj', $items);
        $view->display('obj/simple.php');
    }

    public function action_count($where=[]){
        $cityId=new City();
        $cityId = $cityId->cityNow()->id;
        if(empty($where)) $where=[];
        if($cityId!=0)$where['city_id'] = $cityId;
        return ObjModel::getNumAll($where);
    }
    public function action_text(){
        if(!empty($_GET['city'])) {
            $cityNowId = \App\Model\City::getBy('alias',$_GET['city'])->id;
            $where['city'] = $cityNowId;
        } else {
            $where['city']=0;
        }
        $items = TextModel::getBy('city',$where['city']);
        if(!$items) $items = TextModel::getBy('city','0');
        $view = new View();
        $view->assign('text', $items);
        $view->display('text/main.php');
    }
}