<?php

namespace App\Controller;
use App\Model\MassMedia as MassMediaModel;
use App\Model\Text as TextModel;
use App\View\View;

class Massmedia
{
    public function action_all($where=[],$sorting=[]){
        $items = MassMediaModel::getAll($where,$sorting);
        $view = new View();
        $view->assign('massmedia', $items);
        $view->assign('pageTitle', 'СМИ о нас :: ');
        $view->assign('display', 'massmedia/all.php');
        $view->template();
    }
    public function action_view(){
        $items = MassMediaModel::getOne((int)$_GET['id']);
        $view = new View();
        $view->assign('massmedia', $items[0]);
        $view->assign('pageTitle', $items[0]->title . ' :: СМИ о нас :: ');
        $view->assign('display', 'massmedia/one.php');
        $view->template();
    }

    public function action_some($num,$where=[],$sorting=[]){
        $items = MassMediaModel::getSome((int)$num,$where,$sorting);
        $view = new View();
        $view->assign('massmedia', $items);
        $view->display('massmedia/simple.php');
    }

    public function action_count(){
        return MassMediaModel::getNumAll();
    }

    public function action_text(){
        if(!empty($_GET['city'])) {
            $cityNowId = \App\Model\City::getBy('alias',$_GET['city'])->id;
            $where['city'] = $cityNowId;
        } else {
            $where['city']=0;
        }
        $items = TextModel::getBy('city',$where['city']);
        if(!$items) $items = TextModel::getBy('city','0');
        $view = new View();
        $view->assign('text', $items);
        $view->display('text/main.php');
    }

}