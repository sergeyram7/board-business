<?php

namespace App\Controller;
use App\Model\Video as VideoModel;
use App\Model\Text as TextModel;
use App\View\View;

class Video
{
    public function action_all($where=[],$sorting=[]){
        $items = VideoModel::getAll($where,$sorting);
        $view = new View();
        $view->assign('video', $items);
        $view->assign('pageTitle', 'Видео отзывы :: ');
        $view->assign('display', 'video/all.php');
        $view->template();
        //$view->display('video/all.php');
    }
    public function action_some($num,$where=[],$sorting=[]){
        $items = VideoModel::getSome((int)$num,$where,$sorting);
        $view = new View();
        $view->assign('video', $items);
        $view->display('video/some.php');
    }

    public function action_count(){
        return VideoModel::getNumAll();
    }

    public function action_text(){
        if(!empty($_GET['city'])) {
            $cityNowId = \App\Model\City::getBy('alias',$_GET['city'])->id;
            $where['city'] = $cityNowId;
        } else {
            $where['city']=0;
        }
        $items = TextModel::getBy('city',$where['city']);
        if(!$items) $items = TextModel::getBy('city','0');
        $view = new View();
        $view->assign('text', $items);
        $view->display('text/main.php');
    }
}